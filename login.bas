﻿Type=Class
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "login"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = ""
	' your own variables
	Dim sql,sql1, sql2 As SQL
	Dim myToastId As Int = 1
	Dim cantpag As Int =10
	Dim navegador As String = ""
	Dim navegatorcheck As Future 
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------
	Log("Connected")
	ws = WebSocket1
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
		' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
		page.Refresh
		page.FinishedLoading
	Else
		If page.WebsocketReconnected Then
			Log("Websocket reconnected")
			' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
			' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
			ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
		Else
			' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
			Log("Websocket first connection")
			page.Prepare
			ConnectPage
		End If
	End If
	Log(ABMPageId)
	'----------------------MODIFICATION-------------------------------
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")
End Sub

Sub Page_ParseEvent(Params As Map)
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)
		End Select
	End If
End Sub

public Sub BuildTheme()
	' start with the base theme defined in ABMShared
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	'inp
	theme.AddInputTheme("inp")
	theme.Input("inp").InvalidColor=ABM.COLOR_RED
	theme.Input("inp").FocusForeColor=ABM.COLOR_TRANSPARENT
	theme.Input("inp").Bold=True
	
	
	
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
	'page.AlwaysShowVerticalScrollBar = True
	
		
	' adding a navigation bar
	'ABMShared.BuildNavigationBar(page, "","../images/logo.png", "", "", "")
			
	' create the page grid
	'page.AddRowsM(1,False,0,0, "").AddCellsOSMP(1,1,2,3,8,6,2,300,0,0,0,"").AddCellsOSMP(1,0,0,3,8,6,2,300,0,0,0,"")
	page.AddRows(1,True,"").AddCellsOSMPV(3,0,0,0,12,6,4,10,0,0,0,False,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
	
	'inicio sql
	page.AddExtraJavaScriptFile("custom/bowser.min.js")
	
	
End Sub

public Sub ConnectPage()
	
	
	
	sql.Initialize(Main.strDrv,Main.strUrl)
	sql1.Initialize(Main.strDrv,Main.strUrl)
	sql2.Initialize(Main.strDrv,Main.strUrl)
	
	ws.Session.SetAttribute("IsAuthorized", "inactive")
	ws.Session.SetAttribute("id_usuario","")
	ws.Session.SetAttribute("nom_usuario", "")
	ws.Session.SetAttribute("nom_pais", "")
	ws.Session.SetAttribute("nom_division", "")
	ws.Session.SetAttribute("nom_localidad","")
	ws.Session.SetAttribute("nom_puesto", "")
	ws.Session.SetAttribute("nom_email","")
	ws.Session.SetAttribute("nom_reportedirecto","")
	ws.Session.SetAttribute("nom_puestodirecto", "")
	ws.Session.SetAttribute("nom_emaildirecto", "")
	ws.Session.SetAttribute("nom_clave","")
	ws.Session.SetAttribute("nom_gui","")
	ws.Session.SetAttribute("division","")
	'ws.Session.SetAttribute("periodo","")
	ws.Session.SetAttribute("nom_idioma","esp")
	ws.Session.SetAttribute("gui","")
	ws.Session.setAttribute("nom_reporteindirecto","")
	ws.Session.setAttribute("nom_puestoindirecto","")
	ws.Session.setAttribute("nom_emailindirecto","")
	
	Dim p As Int = 1		
	For p = 1 To cantpag  ' NUMERO DE CAMPOS DE PAGINAS EN LA BD DE PERFILES
			ws.Session.SetAttribute("pagina"&p,"0")
	Next
	
	'	connecting the navigation bar
	'ABMShared.ConnectNavigationBar(page)
	ABMShared.coloreafondo(page)
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","cnt")
	cnt.AddRows(8,True,"").AddCells12(1,"")
	cnt.BuildGrid
	page.Cell(1,2).AddComponent(cnt)
	Dim imglogo As ABMImage
	'imglogo.Initialize(page,"imglogo","../images/fbs.png",3)
	imglogo.Initialize(page,"imglogo","../images/POC.png",3)
	imglogo.IsResponsive=True
	cnt.Cell(1,1).AddComponent(imglogo)

	cnt.Cell(2,1).AddComponent(ABMShared.BuildParagraphc(page,"e1","{B}Usuario / User{/B}" ,ABM.SIZE_H5,"#795548"))
	cnt.Cell(2,1).UseTheme("celdacentro")

	Dim inp1 As ABMInput
	inp1.InitializeWithSize(page,"inp1",ABM.INPUT_TEXT,"",0,0,0,12,12,12,False,"inp")
	inp1.PlaceHolderText = "Correo / Email"
	cnt.Cell(3,1).UseTheme("celdacentrobor")
	cnt.Cell(3,1).MarginTop="-5%"
	inp1.Narrow=True
	cnt.Cell(3,1).AddComponent(inp1)

	cnt.Cell(4,1).AddComponent(ABMShared.BuildParagraphc(page,"e1","{B}Contraseña / Password{/B}" ,ABM.SIZE_H5,"#795548"))
	cnt.Cell(4,1).UseTheme("celdacentro")


	Dim inp2 As ABMInput
	inp2.InitializeWithSize(page,"inp2",ABM.INPUT_PASSWORD,"",0,0,0,12,12,12,False,"inp")
	cnt.Cell(5,1).UseTheme("celdacentrobor")
	cnt.Cell(5,1).MarginTop="-5%"
	inp2.Narrow=True
	cnt.Cell(5,1).AddComponent(inp2)

	Dim btn1 As ABMButton
	btn1.InitializeFlat(page,"btn1","","","Acceder / Login","btngreen")
	cnt.Cell(6,1).UseTheme("celdacentro")
	cnt.Cell(6,1).AddComponent(btn1)
	
	cnt.Cell(7,1).AddComponent(ABMShared.BuildParagraphc(page,"e1","Si olvidó su contraseña haga clic {NBSP}{BR} If you forgot your password click " ,ABM.SIZE_A,"#795548"))
	Dim aqui As ABMLabel
	aqui.Initialize(page,"aqui","{C:#00897b}aquí {BR} here {/C}",ABM.SIZE_A,False,"")
	aqui.Clickable = True
	cnt.Cell(7,1).AddComponent(aqui)
	cnt.Cell(7,1).UseTheme("celdacentro")
	


	' refresh the page http://www.google.com
	page.Refresh
	Dim script As String = $"
		$('button').css('border-radius', '10px');
		$('#cnt').css('border-radius', '10px');
		$('#cnt-r3c1').css('border-radius', '10px');
		$('#cnt-r5c1').css('border-radius', '10px');
							"$
	page.ws.Eval(script,Null)
	
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	'navegatorcheck = ws.EvalWithResult("return bowser.name;", Null)
	
	'Try
	'	navegador = navegatorcheck.Value
	'Catch
	'	Log(LastException)
	'End Try
	
	'Log(navegatorcheck.Value)
	
End Sub


Sub aqui_Clicked(Target As String)
	Dim rst As ResultSet
	Log(Target)
	Dim cnt As ABMContainer=page.Component("cnt")
	Dim inp1 As ABMInput=cnt.Component("inp1")
	Dim user As String=inp1.Text
	Dim existe As Boolean =False
	
	If user <> "" And user <> Null Then
	
		rst = sql1.ExecQuery("select * from k360_usuarios where nom_email collate Latin1_General_CS_AS ='" & user  & "' ")
		Do While rst.NextRow
			existe = True
			
			correoclave(rst.GetString("nom_email"),rst.GetString("nom_usuario"), rst.GetString("nom_clave"))
			page.ShowToast("toast" & myToastId, "toastred", "Se ha reenviado la clave con éxito", 5000)
			
		Loop
		
		If existe = False Then
			If navegador <> "Chrome" And navegador <> "Firefox" Then
				ws.Alert("Debe colocar un correo válido en el usuario")
			Else
				page.Msgbox("msg","Debe colocar un correo válido en el usuario","Error en usuario","Aceptar","")
			End If
		End If
	
	Else
		If navegador <> "Chrome" And navegador <> "Firefox" Then
			ws.Alert("Debe colocar un correo válido en el usuario")
		Else
			page.Msgbox("msg","Debe colocar un correo válido en el usuario","Error en usuario","Aceptar","")
		End If
	End If
	
End Sub


Sub correoclave(cdestino As String, nombre As String, clave As String)
	Dim cuerpo() As String
	Dim subjet As String = "USTED HA RECIBIDO LOS DATOS DE USUARIO PARA KARISMA FEEDBACK 360° / YOU HAVE RECEIVED YOUR LOGIN INFORMATION FOR KARISMA FEEDBACK 360°"
	Dim link As String = "http://karismafeedback360.com:8100/feedback/"
	'	Dim link As String
	'	Dim img As String
	'			#IF RELEASE
	'				link="http://karismafeedback360.com:8100/feedback/evaluacionpub/?id&#61;"&guid&""
	'			 	img="http://karismafeedback360.com:8100/feedback/images/klogo.png"
	'			#ELSE
	'	link="http://localhost:8100/feedback/evaluacionpub/?id&#61;"&guid&""
	'	img="" '"http://localhost:8099/suite/images/klogo.png"
	'			#End If
	cuerpo= Array As String (subjet,nombre,clave, cdestino, link)
						
	Dim destino() As String = Array As String (cdestino)
	'CallSubDelayed3(emailutils,"useremailsend",destino,cuerpo)
	CallSub3(emailutils,"clave360email",destino,cuerpo)
End Sub

Sub btn1_Clicked(Target As String)
	Dim cnt As ABMContainer=page.Component("cnt")
	Dim inp1 As ABMInput=cnt.Component("inp1")
	Dim inp2 As ABMInput=cnt.Component("inp2")
	
	Log(inp1.Text)
	Dim user As String=inp1.Text
	Dim clave As String=inp2.Text
	
	If user ="" Then
		If navegador <> "Chrome" And navegador <> "Firefox" Then
			ws.Alert("Escriba el usuario")
		Else
			page.Msgbox("msg","Escriba el usuario","Error en nombre de usuario","Aceptar","")
		End If	
		Return
	End If
	If clave ="" Then
		
		If navegador <> "Chrome" And navegador <> "Firefox" Then
			ws.Alert("Escriba la clave")
		Else
			page.Msgbox("msg","Escriba la clave","Error en clave de usuario","Aceptar","")
		End If
		
		Return
	End If
	If sql.ExecQuerySingleResult("select count(*) from k360_usuarios where nom_email collate Latin1_General_CS_AS ='" & user  & "'")>0 Then
		If sql1.ExecQuerySingleResult("select count(*) from k360_usuarios where nom_email collate Latin1_General_CS_AS ='" & user  & "' and nom_clave collate Latin1_General_CS_AS ='" & clave & "'")>0 Then
		
			Dim rst As ResultSet
			rst = sql1.ExecQuery("select * from k360_usuarios, k360_perfiles where k360_perfiles.id=id_perfil and nom_email collate Latin1_General_CS_AS ='" & user  & "' and nom_clave collate Latin1_General_CS_AS ='" & clave & "'")
			Do While rst.NextRow	
				ws.Session.SetAttribute("IsAuthorized", "active")		
				ws.Session.SetAttribute("id_usuario", rst.GetString("id_usuario"))
				ws.Session.SetAttribute("nom_usuario", rst.GetString("nom_usuario"))
				ws.Session.SetAttribute("nom_pais", rst.GetString("nom_pais"))
				ws.Session.SetAttribute("nom_division", rst.GetString("nom_division"))
				ws.Session.SetAttribute("nom_localidad", rst.GetString("nom_localidad"))
				ws.Session.SetAttribute("nom_puesto", rst.GetString("nom_puesto"))
				ws.Session.SetAttribute("nom_email",rst.GetString("nom_email"))
				ws.Session.SetAttribute("nom_reportedirecto",rst.GetString("nom_reportedirecto"))
				ws.Session.SetAttribute("nom_puestodirecto", rst.GetString("nom_puestodirecto"))
				ws.Session.SetAttribute("nom_emaildirecto", rst.GetString("nom_emaildirecto"))
				ws.Session.SetAttribute("nom_clave",rst.GetString("nom_clave"))
				ws.Session.SetAttribute("nom_gui",rst.GetString("nom_gui"))
				ws.Session.SetAttribute("nom_idioma",rst.GetString("nom_idioma"))
				If rst.GetString("nom_reporteindirecto") = Null Then
					ws.Session.setAttribute("nom_reporteindirecto","")
					ws.Session.setAttribute("nom_puestoindirecto","")
					ws.Session.setAttribute("nom_emailindirecto","")
				Else	
					ws.Session.setAttribute("nom_reporteindirecto",rst.GetString("nom_reporteindirecto"))
					ws.Session.setAttribute("nom_puestoindirecto",rst.GetString("nom_puestoindirecto"))
					ws.Session.setAttribute("nom_emailindirecto",rst.GetString("nom_emailindirecto"))
				End If
				
				
				Dim query As String = $"select id from k360_periodos where
						 k360_periodos.fec_ano=${ "'"&DateTime.GetYear(DateTime.Now)&"'" } and
						 k360_periodos.nom_division = ${ "'"&rst.GetString("nom_division")&"'" }
							
				"$
				Dim periodoact As Int =  sql2.ExecQuerySingleResult(query)
				ws.Session.SetAttribute("periodo",periodoact)
				
				Dim p As Int = 1
				
				For p = 1 To cantpag  ' NUMERO DE CAMPOS DE PAGINAS EN LA BD DE PERFILES
					If rst.GetString("st_status") <> Null And rst.GetString("st_status") = "1" Then
						ws.Session.SetAttribute("pagina"&p,rst.GetString("pagina"&p))
					Else	
						ws.Session.SetAttribute("pagina"&p,"0")
					End If
				Next
				
				ws.Session.SetAttribute("configuracion",rst.GetString("configuracion"))
					
			Loop
			
		
			rst.Close
			ABMShared.NavigateToPage(page.ws,page.GetPageID,"../altaobjetivo/")
		
		
		Else
			If navegador <> "Chrome" And navegador <> "Firefox" Then
				ws.Alert("Verifique su clave")
			Else
				page.Msgbox("msg","Verifique su clave","Error en clave de entrada","Aceptar","")
			End If
			
		End If
	Else
		If navegador <> "Chrome" And navegador <> "Firefox" Then
			ws.Alert("Verifique su usuario")
		Else
			page.Msgbox("msg","Verifique su usuario","Error en usuario","Aceptar","")
		End If	
	End If
End Sub



' clicked on the navigation bar
Sub Page_NavigationbarClicked(Action As String, Value As String)
	' saving the navigation bar position
	page.SaveNavigationBarPosition
	If Action = "LogOff" Then
		ABMShared.LogOff(page)
		Return
	End If

	ABMShared.NavigateToPage(ws, ABMPageId, Value)
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

