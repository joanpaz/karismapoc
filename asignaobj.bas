﻿Type=Class
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module

Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "asignaobj"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "15"
	' your own variables		
	Dim myToastId As Int = 1
	Public o(10) As Int ' CONTADOR DE OBJETIVOS EN CADA ENFOQUE
	
	Dim cantobj As List
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto, cmbLocalidad,cmbcto,cmbTipo,cmbNum,cmbnivel,cmbempleados,cmbsupervisores As ABMCombo
	Dim i,j,k As Int
	Dim enfoques As List = Array As String ("ENFOQUE FINANCIERO","ENFOQUE AL CLIENTE","ENFOQUE A LOS PROCESOS","ENFOQUE AL PERSONAL")
	Dim sql1, sql2 As SQL
	Dim rst ,rst2 As ResultSet
	Dim obj As Int = 4 'objetivos por renglon
	Dim enf As Int = 4 'cantidad de enfoques
	#IgnoreWarnings:9,10,12,24

End Sub
'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
		End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	
		'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
		'Radio
	theme.AddRadioGroupTheme("radio")
	theme.RadioGroup("radio").WithGab = True
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	theme.Label("label1").BackColor = ABM.COLOR_BROWN
    theme.Label("label1").BackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Label("label1").Align = ABM.TEXTALIGN_CENTER
	theme.Label("label1").ZDepth = ABM.ZDEPTH_1
	
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").ZDepth = ABM.ZDEPTH_1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"POC","../images/klogo3.png","","POC","")
	page.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	page.AddRows(4,True,"").AddCellsOSMP(4,0,0,0,12,6,4,0,0,0,0,"")
	
	page.AddRows(200,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()

	

	Dim pos As Int=0
	sql1.Initialize(Main.strDrv,Main.strUrl)
	sql2.Initialize(Main.strDrv,Main.strUrl)
	
	enf =  sql1.ExecQuerySingleResult("select count(*) from [K360].[dbo].POC_enfoques  where nom_idioma='ESP'")
	
	
	Dim oi As Int
	For oi = 1 To 9 ' ACEPTA HASTA 9 ENFOQUES EN LA BD
		o(oi) = 1
	Next
	
	
	
	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","")
	cnt.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,10,10,10,5,0,10,0,"").AddCellsOSMPV(1,0,0,0,1,1,1,10,0,0,0,ABM.VISIBILITY_HIDE_ON_SMALL_ONLY,"")
	
	cnt.BuildGrid
	page.Cell(1,1).MarginTop="1%"
	page.Cell(1,1).AddComponent(cnt)

	Dim lblt1 As ABMLabel
'	If idmX = "ING" Then
'		lblt1.Initialize(page,"iblt1","{C:#795548}Welcome to the evaluation system{/C}",ABM.SIZE_H5,False,"justifybr")
'	Else
		lblt1.Initialize(page,"iblt1","{C:#795548}Asignación de objetivos POC{/C}",ABM.SIZE_H5,False,"justifybr")
'	End If
'	
	'lblt1.IsFlowText=True
	cnt.Cell(1,1).AddComponent(lblt1)
	

	Dim imglogo As ABMImage
	imglogo.Initialize(page,"iml","../images/POC.png",3)
	imglogo.IsResponsive=True
	cnt.Cell(1,2).AddComponent(imglogo)
	cnt.Cell(1,2).SetFixedHeight(50,False)
	

	page.AddModalSheetTemplate(Mensajes)
	
	Dim contenedora As ABMContainer
	contenedora.Initialize(page, "contenedora", "cardtheme3")
	contenedora.AddRows(3,True,"").AddCellsOS(3,0,0,0,12,6,3,"")
	contenedora.BuildGrid
	page.Cell(2,1).AddComponent(contenedora)
	page.Cell(2,1).SetOffsetSize(0,0,0,12,12,12)
	
	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
	contenedora.Cell(1,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(1,1).Marginleft="2%"
	contenedora.Cell(1,1).AddComponent(cmbDivision)
	
	
	rst=sql1.ExecQuery("Select distinct nom_division from [K360].[dbo].K360_usuarios order by nom_division")
	Do While rst.NextRow
		cmbDivision.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close


	'Localidad
	cmbLocalidad.Initialize(page,"cmbLocalidad","Seleccione una localidad",250,"combo")
	contenedora.Cell(1,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(1,2).Marginleft="2%"
	contenedora.Cell(1,2).AddComponent(cmbLocalidad)
	
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione un Puesto",250,"combo")
	contenedora.Cell(1,3).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(1,3).Marginleft="2%"
	contenedora.Cell(1,3).AddComponent(cmbPuesto)
	
	'Empleados
	cmbempleados.Initialize(page,"cmbempleados","Seleccione un empleado",250,"combo")
	contenedora.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,1).Marginleft="2%"
	contenedora.Cell(2,1).AddComponent(cmbempleados)
	
	
		'supervisores
	cmbsupervisores.Initialize(page,"cmbsupervisores","Seleccione un supervisor",250,"combo")
	contenedora.Cell(2,2).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(2,2).Marginleft="2%"
	contenedora.Cell(2,2).AddComponent(cmbsupervisores)
	
	
	
	
	Dim year As ABMCombo
	year.Initialize(page,"year", "Seleccione el año", 250,"combo")
	contenedora.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
	contenedora.Cell(3,1).Marginleft="2%"
	contenedora.Cell(3,1).AddComponent(year)
	
	
	
	
	For k=2017 To 2099 
	
		year.AddItem(k,k, BuildSimpleItem(k,"",k))	
		
	Next
	
	year.Refresh
	
	
	Dim rbgroup As ABMRadioGroup
		rbgroup.Initialize(page, "rbgroup", "radio")
		rbgroup.AddRadioButton("Anual", True)
		rbgroup.AddRadioButton("Semestre I", True)
		rbgroup.AddRadioButton("Semestre II", True)
		rbgroup.SetActive(0) 

	contenedora.Cell(3,2).AddComponent(rbgroup)
	contenedora.Cell(3,2).Marginleft="2%"
	
	

	Dim fila As Int = 6
	'	Dim cmbobjetive As ABMCombo
	Dim i As Int = 1
	
	rst =  sql1.ExecQuery("select * from [K360].[dbo].POC_enfoques  where nom_idioma='ESP'")
	
	Do While rst.NextRow
		'linea 1
						
		Dim contenedorb As ABMContainer
		contenedorb.Initialize(page, "contenedorb"&i, "")
		contenedorb.AddRows(1,True,"").AddCellsOS(2,0,0,0,6,6,6,"")
		contenedorb.BuildGrid
					
		Dim subtitulo As ABMLabel
		subtitulo.Initialize(page,"subtitulo"&i,rst.GetString("nom_nombre"),ABM.SIZE_PARAGRAPH, False,"label1")
		contenedorb.Cell(1,1).SetOffsetSize(0,0,0,12,6,3)
		contenedorb.Cell(1,1).AddComponent(subtitulo)
		contenedorb.Cell(1,1).Marginleft="2%"
								
		Dim pointsf As ABMCombo
		pointsf.Initialize(page,"pointsf"&i, "Selecciones los puntos (total 100)", 250,"combo")

		contenedorb.Cell(1,2).SetOffsetSize(0,0,0,12,6,3)
		contenedorb.Cell(1,2).AddArrayComponent(pointsf,"PE")
		contenedorb.Cell(1,2).Marginleft="2%"
				
		For k=5 To 100 Step 5
			pointsf.AddItem(i&"-"&k,k, BuildSimpleItem(k,"",k&"%"))
		Next
		pointsf.Refresh
								
						
		page.Cell(fila,1).AddComponent(contenedorb)
											
		fila = fila +1
										
		
		For j = 1 To obj
													
								
			Dim contenedor As ABMContainer
			contenedor.Initialize(page, "contenedor"&i&j, "")
								
			contenedor.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,10,0,"").AddCellsOSMP(1,0,0,0,12,4,4,0,0,10,0,"").AddCellsOSMP(1,0,0,0,12,4,3,0,0,10,0,"")
			contenedor.BuildGrid
								
			If j > 1 Then
				contenedor.CloseContent
						
			Else
				contenedor.OpenContent
			End If
								
			Dim cmbobjetive As ABMCombo
			cmbobjetive.Initialize(page,"O"&i&j, "Seleccione un objetivo", 250,"combo")
								

			contenedor.Cell(1,1).AddArrayComponent(cmbobjetive,"cmbobjetive")
			contenedor.Cell(1,1).Marginleft="2%"
			
			rst2=sql2.ExecQuery("Select * from [K360].[dbo].POC_objetivos where id_enfoque='"&rst.GetString("id")&"' and nom_idioma='"&"ESP"&"' order by nom_nombre ")

			pos=0
			Do While rst2.NextRow
				cmbobjetive.AddItem("O"&i&j&"P"& rst2.GetString2(0),rst2.GetString2(1),BuildSimpleItem("P"& pos,"",rst2.GetString2(1)))
				pos=pos+1
			Loop
			rst2.Close
								
			Dim objetivet As ABMInput
			objetivet.Initialize(page,"objetivet"&i&j,ABM.INPUT_TEXT,"",False,"")
								
			contenedor.Cell(1,1).AddComponent(objetivet)
			
								
			Dim measure As ABMInput
			measure.Initialize(page,"measure"&i&j,ABM.INPUT_TEXT,"¿Como debe ser medido?",True,"input1")

			contenedor.Cell(1,2).AddComponent(measure)
			contenedor.Cell(1,2).Marginleft="2%"
								
			Dim pointso As ABMCombo
			pointso.Initialize(page,"pointso"&i&j, "Seleccione los puntos (parcial)", 250,"combo")
						

			contenedor.Cell(1,3).AddArrayComponent(pointso,"PO")
			contenedor.Cell(1,3).Marginleft="2%"
				
			For k=5 To 100 Step 5
				pointso.AddItem(i&"-"&j&"-"&k,k, BuildSimpleItem(k,"",k&"%"))
			Next
			pointso.Refresh
								
			page.Cell(fila,1).AddComponent(contenedor)
								
			fila = fila +1
			
			
		Next
						
						
						
		'linea 6
						
						
		Dim contenedorc As ABMContainer
		contenedorc.Initialize(page, "contenedorc"&i, "")
		contenedorc.AddRows(1,True,"").AddCellsOS(2,0,0,0,6,6,6,"")
		contenedorc.BuildGrid
						
		Dim btnaggobj As ABMButton
		btnaggobj.InitializeFlat(page,"b"&i,"mdi-content-add", ABM.ICONALIGN_LEFT ,"Agregar","btngreen")
		'page.Cell(i+fila,1).SetOffsetSize(0,0,0,10,3,3)
		'page.Cell(i+fila,1).AddComponent(btnaggobj)
					
		Dim btnrestobj As ABMButton
		btnrestobj.InitializeFlat(page,"b"&i,"mdi-content-remove", ABM.ICONALIGN_LEFT ,"Remover","btngreen")
		'page.Cell(i+fila,2).SetOffsetSize(0,0,0,10,3,3)
		'page.Cell(i+fila,2).AddComponent(btnrestobj)
						
		contenedorc.Cell(1,1).AddarrayComponent(btnaggobj,"btna")
		contenedorc.Cell(1,1).SetOffsetSize(0,0,3,10,3,3)
		contenedorc.Cell(1,2).AddarrayComponent(btnrestobj,"btnr")
		contenedorc.Cell(1,2).SetOffsetSize(0,0,3,10,3,3)
						
		contenedorc.Cell(1,1).SetFixedHeight(50,False)
		contenedorc.Cell(1,2).SetFixedHeight(50,False)
		page.Cell(fila,1).AddComponent(contenedorc)
						
		fila = fila + 1
		i = i + 1
	Loop
	
	rst.close
					
		
		Dim contenedord As ABMContainer
		contenedord.Initialize(page, "contenedord"&i, "")
		contenedord.AddRows(1,True,"").AddCellsOS(3,0,0,0,6,6,6,"")
		contenedord.BuildGrid
		
		Dim salvar As ABMButton
		salvar.InitializeFlat(page,"salvar","", ABM.ICONALIGN_LEFT ,"Salvar","btn")
		'contenedord.Cell(1,1).SetOffsetSize(0,0,3,10,3,3)
		contenedord.Cell(1,1).AddComponent(salvar)
		contenedord.Cell(1,1).UseTheme("celdacentro")
		
		Dim cancelar As ABMButton
		cancelar.InitializeFlat(page,"cancelar","", ABM.ICONALIGN_LEFT ,"Cancelar","btn")
		'contenedord.Cell(1,2).SetOffsetSize(0,0,3,10,3,3)
		contenedord.Cell(1,2).AddComponent(cancelar)
		contenedord.Cell(1,2).UseTheme("celdacentro")
		
'		Dim template As ABMButton
'		template.InitializeFlat(page,"template","", ABM.ICONALIGN_LEFT ,"Salvar modelo","btn")
'		'contenedord.Cell(1,3).SetOffsetSize(0,0,3,10,3,3)
'		contenedord.Cell(1,3).AddComponent(template)
		
		
		page.Cell(fila,1).MarginTop = "2%"
		page.Cell(fila,1).SetOffsetSize(0,0,0,12,12,12)
		page.Cell(fila,1).AddComponent(contenedord)
	
'	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
	
	Dim script As String = $"
		$('button').css('border-radius', '10px');
		$('div').css('border-radius', '10px');
		$('p').css('border-radius', '10px');
		
			$("#nav1TI1icon").removeClass("fa");
		$("#nav1TI1icon").removeClass("fa-flag");
		$("#nav1TI1icon").html('<img alt="" id="extracontent-ing" src="../images/ing360.png" style="opacity: 3.0 ;cursor: pointer;" width="25px" height="25px">');
		
		$("#nav1TI2icon").removeClass("fa");
		$("#nav1TI2icon").removeClass("fa-flag");
		$("#nav1TI2icon").html('<img alt="" id="extracontent-ing" src="../images/esp360.png" style="opacity: 3.0 ;cursor: pointer;" width="25px" height="25px">');
		
		$("#nav1TI3icon").removeClass("fa");
		$("#nav1TI3icon").removeClass("fa-flag");
		$("#nav1TI3icon").html('<img alt="" id="extracontent-ing" src="../images/salir1.png" style="opacity: 3.0 ;cursor: pointer;" width="25px" height="25px">');
		
			 							"$
	
	page.ws.Eval(script,Null)
	
	
	
End Sub

Sub PE_Clicked(Itemid As String)
	
	Dim partes() As String
	partes = Regex.Split("-", Itemid)
	
	Dim CN As ABMContainer = page.Component("contenedorb"&partes(0))
	Dim cact As ABMCombo = CN.cell(1,2).Component("PEpointsf"&partes(0))
		
	Dim c(enf+1) As ABMCombo
	Dim pt As Int = 0
	
'	Dim cact As ABMCombo = CN.cell(1,2).Component(itemId)
'	Dim pcact As Int = cact.GetActiveItemId.Replace("%","")
	
	For i =1 To enf
		 Dim CN As ABMContainer = page.Component("contenedorb"&i)
		 c(i) = CN.cell(1,2).Component("PEpointsf"&i)
		If c(i).GetActiveItemId <> -1 Then
			Dim p() As String = Regex.Split("-", c(i).GetActiveItemId)
			pt = pt + p(1)
		End If
	Next
	
	If pt > 100 Then
		ws.Alert("La suma de los enfoques no debe exceder de 100%")
		cact.SetActiveItemId(-1)
		cact.Refresh
		Return	
	
	Else
		
		For i =1 To enf
			Dim CN As ABMContainer = page.Component("contenedorb"&i)
			c(i) = CN.cell(1,2).Component("PEpointsf"&i)
			c(i).SetTooltip("Restan "&(100-pt)&" puntos por asignar",ABM.TOOLTIP_RIGHT,1000)
			c(i).Title="Seleccione los puntos (restan "&(100-pt)&")"
			c(i).Refresh
		Next
		
		
	End If
	
	Dim script As String = $"
		$('div').css('border-radius', '10px');
		 							"$
	
	page.ws.Eval(script,Null)
	
	
End Sub

Sub PO_Clicked(itemId As String)
	Dim partes() As String
	Dim partes2() As String
	partes = Regex.Split("-", itemId)
	
	Dim CN As ABMContainer = page.Component("contenedorb"&partes(0))
	Dim cact As ABMCombo = CN.cell(1,2).Component("PEpointsf"&partes(0))
	partes2 = Regex.Split("-", cact.GetActiveItemId)
	
	Dim contenedor As ABMContainer = page.Component("contenedor"&partes(0)&partes(1))
	Dim cacto As ABMCombo=contenedor.Cell(1,3).Component("POpointso"&partes(0)&partes(1))
	
	Dim c(o(partes(0))+1) As ABMCombo
	Dim pt As Int = 0
	
	'	Dim cact As ABMCombo = CN.cell(1,2).Component(itemId)
	'	Dim pcact As Int = cact.GetActiveItemId.Replace("%","")
	
	For i =1 To o(partes(0))
		Dim contenedor As ABMContainer = page.Component("contenedor"&partes(0)&i)
		c(i) = contenedor.Cell(1,3).Component("POpointso"&partes(0)&i)
		If c(i).GetActiveItemId <> -1 Then
			Dim p() As String = Regex.Split("-", c(i).GetActiveItemId)
			pt = pt + p(2)
		End If
	Next
	
	If pt > partes2(1) Then
		ws.Alert("La suma de los objetivos no debe exceder el porcentaje total del enfoque")
		cacto.SetActiveItemId(-1)
		cacto.Refresh
		Return
	
	Else
		
		For i =1 To o(partes(0))
			Dim contenedor As ABMContainer = page.Component("contenedor"&partes(0)&i)
			c(i) = contenedor.Cell(1,3).Component("POpointso"&partes(0)&i)
			c(i).SetTooltip("Restan "&(partes2(1)-pt)&" puntos por asignar",ABM.TOOLTIP_RIGHT,1000)
			c(i).Title="Seleccione los puntos (restan "&(partes2(1)-pt)&")"
			c(i).Refresh
		Next
		
		
	End If
	
	Dim script As String = $"
		
		$('div').css('border-radius', '10px');
			
			 							"$
	
	page.ws.Eval(script,Null)
	
	
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub



Sub cmbobjetive_Clicked(itemId As String)
	
	
	Dim nomcombo As String = itemId.SubString2(0,3)
	Dim id As String = itemId.SubString2(1,3)
	
	
	Dim container As ABMContainer = page.Component("contenedor"&id)
	Dim combo As ABMCombo = container.Cell(1,1).Component("cmbobjetive"&nomcombo)
	Dim input As ABMInput = container.Cell(1,1).Component("objetivet"&id)
	Dim label As ABMLabel= combo.GetComponent(itemId)
	input.Text = label.Text
	input.Refresh
	
End Sub


Sub cmbDivision_Clicked(itemId As String)
	
	Dim cont As ABMContainer = page.Component("contenedora")
	Dim cmby As ABMCombo=cont.cell(1,2).Component("cmbLocalidad")
	cmby.Clear
	cmby.Refresh

	Dim pos As Int=0
	
	rst=sql1.ExecQuery("Select distinct nom_localidad from K360_usuarios where nom_division='" & itemId & "' order by nom_localidad")
	cmby.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmby.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	page.resume
	rst.Close
	cmby.Refresh
	
	Dim script As String = $"
		$('div').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
	
End Sub

Sub cmbLocalidad_Clicked(itemId As String)
	Dim cont As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=cont.cell(1,1).Component("cmbDivision")
	Dim cmby As ABMCombo=cont.cell(1,3).Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh

	Dim pos As Int=0
	
	rst=sql1.ExecQuery("Select distinct nom_puesto from K360_usuarios where nom_division='" & cmbx.GetActiveItemId & "' and nom_localidad='" & itemId & "' order by nom_puesto")
	cmby.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmby.AddItem(rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	page.resume
	rst.Close
	cmby.Refresh
	
	Dim script As String = $"
		$('div').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
End Sub

Sub cmbPuesto_Clicked(itemId As String)
	Dim cont As ABMContainer = page.Component("contenedora")
	Dim cmbx As ABMCombo=cont.cell(1,1).Component("cmbDivision")
	Dim cmby As ABMCombo=cont.cell(1,2).Component("cmbLocalidad")
	Dim cmbz As ABMCombo=cont.cell(2,1).Component("cmbempleados")
	cmbz.Clear
	cmbz.Refresh

	Dim pos As Int=0
	page.Pause
	
	rst=sql1.ExecQuery("Select * from K360_usuarios where nom_puesto='"&itemId&"'  and nom_localidad='"&cmby.GetActiveItemId&"' and nom_division='"&cmbx.GetActiveItemId&"' ")
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
	
	Dim script As String = $"
		$('div').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
End Sub

Sub cmbempleados_Clicked(itemId As String)
	Dim cont As ABMContainer = page.Component("contenedora")
	Dim cmbz As ABMCombo=cont.cell(2,2).Component("cmbsupervisores")
	cmbz.Clear
	cmbz.Refresh

	Dim pos As Int=0
	page.Pause
	
	rst=sql1.ExecQuery("Select nom_reportedirecto from K360_usuarios where id_usuario='"&itemId&"' ")
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
	
	Dim script As String = $"
		$('div').css('border-radius', '10px');
			 							"$
	
	page.ws.Eval(script,Null)
	
End Sub




Sub btna_Clicked(Target As String) 
	
	
	Dim i As Int = Target.SubString2(5,6)
	
	
  	If o(i) < 4 Then
		 o(i) = o(i) + 1
     Dim contenedor As ABMContainer = page.Component("contenedor"&i&o(i)) 
	 contenedor.OpenContent
 	' contenedor.RunAnimation("unblur")
'	 contenedor.Cell(1,1).SetFixedHeight(95,False)
'	 contenedor.Cell(1,2).SetFixedHeight(95,False)
'	 contenedor.Cell(1,3).SetFixedHeight(95,False)


 	 contenedor.Refresh	
	Else
	End If
	
	Dim script As String = $"

		$('div').css('border-radius', '10px');
		
			 							"$
	
	page.ws.Eval(script,Null)
 End Sub



Sub btnr_Clicked(Target As String)
	
	Dim i As Int = Target.SubString2(5,6)
	
	
	If o(i) > 1 Then
     Dim contenedor As ABMContainer = page.Component("contenedor"&i&o(i)) 
 	 contenedor.CloseContent
	 'contenedor.RunAnimation("blur")
	 'contenedor.Cell(1,1).SetFixedHeight(1,False)
	 'contenedor.Cell(1,2).SetFixedHeight(1,False)
	 'contenedor.Cell(1,3).SetFixedHeight(1,False)
 	 contenedor.Refresh	
	 o(i) = o(i) - 1	
 	Else
	End If
	
	Dim script As String = $"

		$('div').css('border-radius', '10px');
		
			 							"$
	
	page.ws.Eval(script,Null)
	
End Sub




Sub salvar_Clicked(Target As String)
	

		Dim valores(), valores2()  As Object
		Dim pos, pos2 As Int=0
		Dim idobj , idmed As Int
		Dim sqlt As String 
		
   
'	Dim C0 As ABMCombo=page.Component("cmbSubDivision")
'	Dim T0 As String=C0.GetActiveItemId
'	Dim L0 As ABMLabel=C0.GetComponent(T0)
	
	Dim C1 As ABMCombo=page.Component("year")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)

	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
	Dim periodo As Int = R1.GetActive 
	Dim P1 As String = periodo
	
	Dim C2 As ABMCombo=page.Component("cmbempleados")
	Dim T2 As String=C2.GetActiveItemId
	Dim L2 As ABMLabel=C2.GetComponent(T2)
		
	'OBJETIVO 4
	'FOCUS  5
	'Puntos del onjetivo 6
	'EVAL   7 ""
	'COMEN  8 ""
	
	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
	Dim T3 As String=C3.GetActiveItemId
	Dim L3 As ABMLabel=C3.GetComponent(T3)
		
	'Puntos del focus 10
	'Puntos totales adquiridos 11
	'Measure 12
	'Obj txt 13
	
	Dim C4 As ABMCombo=page.Component("cmbDivision")
	Dim T4 As String=C4.GetActiveItemId
	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbLocalidad")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)
	
	Dim C6 As ABMCombo=page.Component("cmbPuesto")
	Dim T6 As String=C6.GetActiveItemId
	Dim L6 As ABMLabel=C6.GetComponent(T6)

	
	' BORRADO TEMPORAL HASTA QUE SE REALICE EL UPDATE
	
'	sqlt = "Delete from RH_ObjEval where User_Id='"& T2 &"' and User_Id_Eval='"& T3 &"'"
'	sql1.ExecNonQuery(sqlt)

	''page.pause
	
	
	Dim query As String = $"select id from k360_periodos where
						 k360_periodos.fec_ano=${ "'"&DateTime.GetYear(DateTime.Now)&"'" } and
						 k360_periodos.nom_division = ${ "'"&T4&"'" }
							
				"$
	Dim periodoact As Int =  sql1.ExecQuerySingleResult(query)
	
	valores= Array As Object (periodoact,T2,Null,T3,Null,DateTime.Date(DateTime.Now),DateTime.Date(DateTime.Now),"1")
	sql1.ExecNonQuery2("INSERT INTO POC_evaluaciones VALUES (?,?,?,?,?,?,?,? )", valores)
	
	
	
	Dim evalid As String =sql1.ExecQuerySingleResult("SELECT IDENT_CURRENT('POC_evaluaciones')")
	
	rst =  sql1.ExecQuery("select * from [K360].[dbo].POC_enfoques  where nom_idioma='ESP'")
	Dim i As Int = 1
	Do While rst.NextRow
		
		
				Dim CN As ABMContainer = page.Component("contenedorb"&i) 
				Dim C6 As ABMCombo= CN.cell(1,2).Component("pointsf"&i)
				Dim T6 As String=C6.GetActiveItemId
				Dim L6 As ABMLabel=C6.GetComponent(T6)
				
		For j = 1 To o(i)
				

				Dim contenedor As ABMContainer = page.Component("contenedor"&i&j)

				Dim C7 As ABMCombo=contenedor.Cell(1,1).Component("cmbobjetive"&"O"&i&j)
				Dim T7 As String=C7.GetActiveItemId
				Dim L7 As ABMLabel	
				
				If Not(T7 = -1) Then
					L7=C7.GetComponent(T7)
				Else
					L7=Null	
				End	if
				
				Dim Tx1 As ABMInput = contenedor.Cell(1,2).Component("measure"&i&j)

				Dim C8 As ABMCombo=contenedor.Cell(1,3).Component("pointso"&i&j)
				Dim T8 As String=C8.GetActiveItemId
				Dim L8 As ABMLabel=C8.GetComponent(T8)
	
				Dim objnew As ABMInput = contenedor.Cell(1,1).Component("objetivet"&i&j)
				
				''''''''''''''''''''''' REVISA SI EL OBJETIVO EXISTE O LO GUARDA
				If Not(objnew.Text = "") And (L7=Null) Then
				
				' and Dept_Key='"& T4 &"'
				sqlt = "Select * from POC_objetivos where nom_nombre='" & objnew.Text & "' and nom_puesto='"&T6&"'  and nom_localidad='"&T5&"' and nom_division='"&T4&"' and nom_idioma='"&"ESP"&"'"
						
					rst2=sql2.ExecQuery(sqlt)	
					pos = 0
					Do While rst2.NextRow
					 pos = 1
						Dim getinobj As Int = rst2.GetInt2(0)
					Loop
					rst2.Close
					
						
					If pos = 0 Then	
						
					valores2= Array As Object (objnew.Text,"ESP",Null,T4,T5,T6,rst.GetString("id"),(L8.Text.Replace("%","")/100))
						sql2.ExecNonQuery2("INSERT INTO POC_objetivos VALUES (?,?,?,?,?,?,?,? )", valores2)
						
						rst2=sql2.ExecQuery(sqlt)
						
						Do While rst2.NextRow
							idobj = rst2.GetInt2(0)	
							pos=pos+1
						Loop
						rst2.Close
						
						
					Else	
						
						idobj = getinobj
							
					End If
										
				Else
					
					idobj = T7.Replace("O"&i&j&"P","")
					
					
				End If		
				

			
			
			
			''''''''''''''''''''''' REVISA SI EL LA MEDICION EXISTE PARA SU GUARDADO
			
			If Not(Tx1.Text = "")Then
				
				' and Dept_Key='"& T4 &"'
				sqlt = "Select * from POC_mediciones where nom_nombre='" & Tx1.Text & "' and nom_puesto='"&T6&"'  and nom_localidad='"&T5&"' and nom_division='"&T4&"' and nom_idioma='"&"ESP"&"'"
					
				rst2=sql2.ExecQuery(sqlt)
				pos2 = 0
				Do While rst2.NextRow
					pos2 = 1
					Dim idmedicion As Int = rst2.GetInt2(0)
				Loop
				rst2.Close
					
						
				If pos2 = 0 Then
						
					valores2= Array As Object (Tx1.Text,"ESP",Null,T4,T5,T6,rst.GetString("id"),(L6.Text.Replace("%","")/100))
					sql2.ExecNonQuery2("INSERT INTO POC_mediciones VALUES (?,?,?,?,?,?,?,? )", valores2)
						
					rst2=sql2.ExecQuery(sqlt)
						
					Do While rst2.NextRow
						idmed = rst2.GetInt2(0)
						pos2=pos2+1
					Loop
					rst2.Close
						
						
				Else
						
					idmed = idmedicion
							
				End If
										
			Else
					
				'MEDICION VACIA'
					
					
			End If
				
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
					
						
			
			valores=Array As Object  (evalid,rst.GetString("id"),(L8.Text.Replace("%","")/100),idobj, objnew.Text,(L6.Text.Replace("%","")/100),idmed,Tx1.Text,Null,Null,Null)
			sql1.ExecNonQuery2("INSERT INTO POC_evaluaciones_det VALUES (?,?,?,?,?,?,?,?,?,?,? )", valores)	
							
		Next
	i = i + 1	
	Loop
	rst.Close
	page.Resume
	page.ShowToast("toast1", "toastgreen", "GUARDADO FINALIZADO", 5000)
	Log ("FINALIZANDO GUARDADO")
	'ABMShared.NavigateToPage(page.ws, page.GetPageID, "../asignaobj/asignaobj.html")
	
'	Dim C1 As ABMCombo=page.Component("year")
'	Dim T1 As String=C1.GetActiveItemId
'	Dim L1 As ABMLabel=C1.GetComponent(T1)
'	
'	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
'	Dim periodo As Int = R1.GetActive 
'	Dim p1 As String = periodo
'	
'	
'	Dim C2 As ABMCombo=page.Component("cmbempleados")
'	Dim T2 As String=C2.GetActiveItemId
'	Dim L2 As ABMLabel=C2.GetComponent(T2)
'	
'	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
'	Dim T3 As String=C3.GetActiveItemId
'	Dim L3 As ABMLabel=C3.GetComponent(T3)
	
	
	
	
End Sub


'Sub template_Clicked(Target As String)
'		
'		Dim sql1 As SQL
'		Dim valores(), valores2() As Object
'		sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
'		Dim rst As ResultSet
'		Dim pos As Int=0
'		Dim idobj As Int
'	
'	Dim sqlt As String
'	
''	Dim C1 As ABMCombo=page.Component("cmbDivision")
''	Dim T1 As String=C1.GetActiveItemId
''	Dim L1 As ABMLabel=C1.GetComponent(T1)
''	
''	Dim C2 As ABMCombo=page.Component("cmbSubDivision")
''	Dim T2 As String=C2.GetActiveItemId
''	Dim L2 As ABMLabel=C2.GetComponent(T2)
''	
''	Dim C3 As ABMCombo=page.Component("cmbLocation")
''	Dim T3 As String=C3.GetActiveItemId
''	Dim L3 As ABMLabel=C3.GetComponent(T3)
'	
'	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
'	Dim T4 As String=C4.GetActiveItemId
'	Dim L4 As ABMLabel=C4.GetComponent(T4)
'	
'	Dim C5 As ABMCombo=page.Component("cmbPuesto")
'	Dim T5 As String=C5.GetActiveItemId
'	Dim L5 As ABMLabel=C5.GetComponent(T5)
'	
'	
'	'limpia el template
'
'	sqlt = "Delete from RH_ObjTemp where Pos_key='"& T5 &"' and Dept_Key='"& T4 &"' and nom_db='"& ws.Session.GetAttribute("DBUse") &"'"
'				
'	sql1.ExecNonQuery(sqlt)
'
'	page.pause
'	For i = 1 To enfoques.Size
'		
'		
'				Dim CN As ABMContainer = page.Component("contenedorb"&i) 
'				Dim C6 As ABMCombo= CN.cell(1,2).Component("pointsf"&i)
'				Dim T6 As String=C6.GetActiveItemId
'				Dim L6 As ABMLabel=C6.GetComponent(T6)
'				
'		For j = 1 To o(i)
'				
'
'				Dim contenedor As ABMContainer = page.Component("contenedor"&i&j)
'
'				Dim C7 As ABMCombo=contenedor.Cell(1,1).Component("cmbobjetive"&"O"&i&j)
'				Dim T7 As String=C7.GetActiveItemId
'				Dim L7 As ABMLabel=C7.GetComponent(T7)
'				Dim obj As String
'				
'				If Not(T7=-1) Then
'				  
'					obj = L7.text
'				Else	
'					obj = ""
'				End If
'				
'				
'				Dim Tx1 As ABMInput = contenedor.Cell(1,2).Component("measure"&i&j)
'
'				Dim C8 As ABMCombo=contenedor.Cell(1,3).Component("pointso"&i&j)
'				Dim T8 As String=C8.GetActiveItemId
'				Dim L8 As ABMLabel=C8.GetComponent(T8)
'	
'	
'				Dim objnew As ABMInput = contenedor.Cell(1,1).Component("objetivet"&i&j)
'				
'				
'				
'				
'				
'				
'				
'				
'				If Not(objnew.Text = obj) And Not(objnew.Text = "")  Then
'				
'										
'				sqlt = "Select * from RH_Objetive where nom_objetive='" & objnew.Text & "' and Pos_key='"& T5 &"' and Dept_Key='"& T4 &"' and nom_db='"& ws.Session.GetAttribute("DBUse") &"' and id_focus='"& i &"' "
'								
'							rst=sql1.ExecQuery(sqlt)	
'							pos = 0
'							Do While rst.NextRow
'							 pos = 1
'							 Dim getobj = rst.GetInt2(0)
'							Loop
'							rst.Close
'							
'								
'							If pos = 0 Then	
'								valores2= Array As Object (objnew.Text,Tx1.Text,i,T5,T4,ws.Session.GetAttribute("DBUse"))
'								sql1.ExecNonQuery2("INSERT INTO RH_Objetive VALUES (?,?,?,?,?,? )", valores2)
'								
'								rst=sql1.ExecQuery(sqlt)
'								
'								Do While rst.NextRow
'									idobj = rst.GetInt2(0)	
'									pos=pos+1
'								Loop
'								rst.Close
'								
'								
'							Else	
'								
'									idobj = getobj
'									
'							End If
'										
'				Else
'					If (objnew.Text = obj) Then
'						idobj = T7.SubString(4)
'					Else
'						Log ("Debe ser llenado el campo de objetivo")
'					End If	
'					
'				End If
'			
'			
'					 valores=Array As Object  (ws.Session.GetAttribute("DBUse"),T4,T5,i,(L6.Text.Replace("%","")/100),idobj,Tx1.Text,(L8.Text.Replace("%","")/100))
'			 		 sql1.ExecNonQuery2("INSERT INTO RH_ObjTemp VALUES (?,?,?,?,?,?,?,? )", valores)	
'			
'				
'		Next
'	Next
'	
'	page.Resume
'	page.ShowToast("toast1", "toastgreen", "GUARDADO FINALIZADO", 5000)
'	Log ("FINALIZANDO GUARDADO")
'	
'	
''	Dim C1 As ABMCombo=page.Component("year")
''	Dim T1 As String=C1.GetActiveItemId
''	Dim L1 As ABMLabel=C1.GetComponent(T1)
''	
''	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
''	Dim periodo As Int = R1.GetActive 
''	Dim p1 As String = periodo
''	
''	
''	Dim C2 As ABMCombo=page.Component("cmbempleados")
''	Dim T2 As String=C2.GetActiveItemId
''	Dim L2 As ABMLabel=C2.GetComponent(T2)
''	
''	Dim C3 As ABMCombo=page.Component("cmbsupervisores")
''	Dim T3 As String=C3.GetActiveItemId
''	Dim L3 As ABMLabel=C3.GetComponent(T3)
'		
'	
'	
'End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista asignaobj")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
