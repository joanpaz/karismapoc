﻿Type=StaticCode
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

Sub Process_Globals
	Private ABM As ABMaterial
	Private sql As SQL
	Private rst As ResultSet
	
	'dropdown
	Public Diseño As String= $"
		$('button').css('border-radius', '10px');
		$('div').css('border-radius', '10px');
		$('p').css('border-radius', '10px');
		$('[id*=-up]').on('keyup', function(event) {
		var $this = $(this),
		val = $this.val();
		$this.val(val.toUpperCase());
		});
		$('[id*=cmb]').css('border-radius','0px');
	 							"$
	'$('[id*=-da]').data('inputmask',"'alias': 'date'");
	'$('[id*=-de]').data('inputmask','mask' :'99-9999999');

End Sub
Sub TituloGrande (texto As String,page As ABMPage) As ABMComponent
	Dim lbl2 As ABMLabel
	lbl2.Initialize(page,"lbl2",texto,ABM.SIZE_H5,False,"lbltitulo")
	Return lbl2
End Sub
Sub TituloMediano (texto As String,page As ABMPage) As ABMComponent
	Dim lbl2 As ABMLabel
	lbl2.Initialize(page,"lbl2",texto,ABM.SIZE_A,False,"lbltitulo")
	Return lbl2
End Sub

Sub InputTexto (Pagina As ABMPage, Nombre As String,id As String,tipo As String ) As ABMComponent
	Dim txt As ABMInput
	txt.Initialize(Pagina,id & tipo,ABM.INPUT_TEXT,Nombre,False,"")
	Return txt
End Sub
Sub InputTextoNw (Pagina As ABMPage, Nombre As String,id As String,tipo As String ) As ABMComponent
	Dim txt As ABMInput
	txt.Narrow=True
	txt.Initialize(Pagina,id & tipo,ABM.INPUT_TEXT,Nombre,False,"")
	Return txt
End Sub

Sub inisql
	sql.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=GIN;user=karismapass;password=SuiteHC$;")
End Sub
Sub Combosql (Pagina As ABMPage,id As String,Titulo As String,Tabla As String,campo1 As String,campo2 As String,filtro As String,orden As String) As ABMComponent
	Dim cmb As ABMCombo
	inisql
	rst=sql.ExecQuery("Select " & campo1 & "," & campo2 & " from " & Tabla & " " & filtro	& orden)
	cmb.Initialize(Pagina,id,Titulo,150,"")
	Do While rst.NextRow
		cmb.AddItem(rst.GetString(campo1),rst.GetString(campo2),(Simplelbl("lbl-s","",rst.GetString(campo1),Pagina)))
	Loop
	Return cmb
End Sub

Sub Combosimple (Pagina As ABMPage,id As String,Titulo As String,valores As Map) As ABMComponent
	Dim cmb As ABMCombo
	cmb.Initialize(Pagina,id,Titulo,150,"")
	For Each v As String In valores.Keys
		cmb.AddItem(v,valores.GetDefault(v,""),(Simplelbl("lbl-s","",valores.GetDefault(v,""),Pagina)))
	Next
	cmb.Narrow=True
	Return cmb
End Sub

Sub Simplelbl(id As String, icon As String, Title As String,pagina As ABMPage) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(pagina, id, Title, ABM.SIZE_H6, True, "header")
	Else
		lbl.Initialize(pagina, id, Title, ABM.SIZE_H6, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub


