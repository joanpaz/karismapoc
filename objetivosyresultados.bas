﻿Type=Class
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "objetivosyresultados"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "11"
	' your own variables		
	Dim myToastId As Int = 1
	'Private combo As ABMCombo
	Dim cmbDivision,cmbSubDivision,cmbLocation,cmbAreas,cmbDepartamento,cmbPuesto,cmbcto,cmbTipo,cmbNum,cmbnivel,cmbempleados,cmbsupervisores As ABMCombo
	Dim calif As List = Array As String ("EE","E","MB","B","R","D")
	Dim califval() As Double  = Array As Double (1.01,0.9,0.8,0.7,0.6,0.5) 
	Public o(5) As Int
	Dim enfoques As List = Array As String ("ENFOQUE FINANCIERO","ENFOQUE AL CLIENTE","EN FOQUE A LOS PROCESOS","ENFOQUE AL PERSONAL")
		Private inputp	As ABMInput	
	Dim sql1, sql2 As SQL
	Dim rst ,rst2 As ResultSet
	#IgnoreWarnings:9,10,12,24
	
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
		'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
	
	
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	
		' add additional themes specific for this page
'	theme.AddContainerTheme("encabezado")
'	theme.Container("encabezado").BackColor = ABM.COLOR_BROWN
	
	
	
	theme.AddCellTheme("encabezadocell")
	theme.Cell("encabezadocell").Align=ABM.CELL_ALIGN_CENTER
	theme.Cell("encabezadocell").BackColor=ABM.COLOR_BROWN
	theme.Cell("encabezadocell").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	theme.Cell("encabezadocell").BorderColor = ABM.COLOR_BLACK
	theme.Cell("encabezadocell").BorderWidth = 1
	
	theme.AddCellTheme("cuerpocell")
	theme.Cell("cuerpocell").Align= ABM.CELL_ALIGN_JUSTIFY
	theme.Cell("cuerpocell").VerticalAlign = True
	
	'theme.Cell("cuerpocell").Align=ABM.CELL_ALIGN_CENTER
	'theme.Cell("cuerpocell").BackColor=ABM.COLOR_BROWN
	'theme.Cell("cuerpocell").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	'theme.Cell("cuerpocell").BorderColor = ABM.COLOR_BLACK
	'theme.Cell("cuerpocell").BorderWidth = 1
	

'	theme.AddRowTheme("encabezadorow")
'	theme.Row("encabezadorow").BackColor=ABM.COLOR_BROWN
'	theme.Row("encabezadorow").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
'	theme.Row("encabezadorow").BorderColor = ABM.COLOR_BLACK
'	theme.Row("encabezadorow").BorderWidth = 2
'	
	
		'tema de celda titulo
	theme.AddCellTheme("titulo1")
 	theme.Cell("titulo1").Align=ABM.CELL_ALIGN_CENTER
 	theme.Cell("titulo1").BackColor=ABM.COLOR_BROWN
 	theme.Cell("titulo1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
 	'theme.Input("input1").   '=ABM.INPUT_TEXTALIGN_CENTER
 	'theme.Input("input1").
 	'theme.Input("input1").BackColorIntensity=ABM.INTENSITY_LIGHTEN1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"POC","../images/klogo3.png","","POC","")

	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(4,True, "").AddCells12(3,"")
	page.AddRows(1,True, "").AddCells12(1,"")
	page.AddRows(1,True, "").AddCells12(1,"")
		
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()
	'	connecting the navigation bar
	ABMShared.ConnectNavigationBar(page)
	Dim lbl As ABMLabel
	lbl.Initialize(page,"lbl","PLAN DE TRABAJO - OBJETIVOS",ABM.SIZE_H5,False,"")
	page.Cell(1,1).AddComponent(lbl)
		
	page.AddModalSheetTemplate(Mensajes)
		
	Dim pos As Int=0
	
	sql1.Initialize(Main.strDrv,Main.strUrl)
	
'	cmbDivision.Initialize(page,"cmbDivision",  "División",150,"combo")
'	cmbDivision.AddItem("P","",BuildSimpleItem("P","",""))
'	page.Cell(2,1).SetOffsetSize(0,0,0,12,6,3)
'	page.Cell(2,1).AddComponent(cmbDivision)
'	If (sql1.IsInitialized)	Then
'		rst=sql1.ExecQuery("Select * from RH_dIVISION order by 1")
'		Do While rst.NextRow
'			cmbDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'			pos=pos+1
'		Loop
'		rst.Close
'	End If
'	
'	'SubDivision	
'	cmbSubDivision.Initialize(page,"cmbSubDivision","Seleccione el país",250,"combo")
'	page.Cell(2,2).SetOffsetSize(0,0,1,12,6,3)
'	page.Cell(2,2).AddComponent(cmbSubDivision)
'	If (sql1.IsInitialized)	Then
'		rst=sql1.ExecQuery("Select * from RH_Country order by 1")
'		cmbSubDivision.AddItem("P","",BuildSimpleItem("P","",""))
'		pos=0
'		Do While rst.NextRow
'			cmbSubDivision.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'			pos=pos+1
'		Loop
'		rst.Close
'	End If	
'	'Location
'	cmbLocation.Initialize(page,"cmbLocation","Seleccione la locación",250,"combo")
'	page.Cell(2,3).SetOffsetSize(0,0,1,12,6,3)
'	page.Cell(2,3).AddComponent(cmbLocation)
	'Areas
'	cmbAreas.Initialize(page,"cmbAreas","Select Area",250,"combo")
'	page.Cell(3,1).SetOffsetSize(0,0,0,12,6,3)
'	page.Cell(3,1).AddComponent(cmbAreas)
'	If (sql1.IsInitialized)	Then
'		rst=sql1.ExecQuery("Select * from RH_Area order by 1")
'		Do While rst.NextRow
'			cmbAreas.AddItem("P"& pos,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
'			pos=pos+1
'		Loop
'		rst.Close
'	End If	
	'Depatamentos
	cmbDepartamento.Initialize(page,"cmbDepartamento","Seleccione el departamento",250,"combo")
	page.Cell(3,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,2).AddComponent(cmbDepartamento)
	If (sql1.IsInitialized)	Then
		rst=sql1.ExecQuery("Select * from [karismaHC].[dbo].RH_Dept")
		pos=0
		cmbDepartamento.AddItem("P","",BuildSimpleItem("P","",""))
		Do While rst.NextRow
			cmbDepartamento.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
			pos=pos+1
		Loop
		rst.close
	End If
	'Puesto
	cmbPuesto.Initialize(page,"cmbPuesto","Seleccione la posición",250,"combo")
	page.Cell(3,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(3,3).AddComponent(cmbPuesto)
	
	'Empleados
	cmbempleados.Initialize(page,"cmbempleados","Seleccione el empleado",250,"combo")
	page.Cell(4,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,1).AddComponent(cmbempleados)
'	If (sql1.IsInitialized)	Then
'		rst=sql1.ExecQuery("Select * from ER_USERS")
'		pos=0
'		cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
'		Do While rst.NextRow
'			cmbempleados.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
'			pos=pos+1
'		Loop
'		rst.close
'	End If
		'supervisores
	cmbsupervisores.Initialize(page,"cmbsupervisores","Seleccione el supervisor",250,"combo")
	page.Cell(4,2).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(4,2).AddComponent(cmbsupervisores)
	cmbsupervisores.AddItem( ws.Session.GetAttribute("idsesion"),ws.Session.GetAttribute("nombre"),BuildSimpleItem("P"& pos,"",ws.Session.GetAttribute("nombre")))
'	If (sql1.IsInitialized)	Then
'		rst=sql1.ExecQuery("Select * from ER_USERS")
'		pos=0
'		cmbsupervisores.AddItem("P","",BuildSimpleItem("P","",""))
'		Do While rst.NextRow
'			cmbsupervisores.AddItem( rst.GetString2(1),rst.GetString2(5),BuildSimpleItem("P"& pos,"",rst.GetString2(5)))
'			pos=pos+1
'		Loop
'		rst.close
'	End If	
	
	Dim Open As ABMButton
	Open.InitializeFlat(page,"Open","", ABM.ICONALIGN_LEFT ,"Abrir","btn")
	page.Cell(5,3).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(5,3).AddComponent(Open)
	
	Open.Tag = "open"
	
	Dim year As ABMCombo
	year.Initialize(page,"year", "Seleccione el año", 250,"combo")
	'page.Cell(5,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(5,1).SetOffsetSize(0,0,1,12,6,3)
	page.Cell(5,1).AddComponent(year)
	
	For k=2000 To 2099 
	
		year.AddItem(k,k, BuildSimpleItem(k,"",k))	
		
	Next
	
	year.Refresh
		
	Dim rbgroup As ABMRadioGroup
		rbgroup.Initialize(page, "rbgroup", "radio")
		rbgroup.AddRadioButton("Anual", True)
		rbgroup.AddRadioButton("Semestre I", True)
		rbgroup.AddRadioButton("Semestre II", True)
		rbgroup.SetActive(0) 
		page.Cell(5,2).SetOffsetSize(0,0,1,12,6,3)
		page.Cell(5,2).AddComponent(rbgroup)
		
	Dim encabezado As ABMContainer
	
	encabezado.Initialize(page,"table","")
	encabezado.AddRows(1,True,"").AddCells12MP(6,0,0,10,10,"encabezadocell")
	encabezado.BuildGrid
	
	
	encabezado.Cell(1,1).SetOffsetSize(0,0,0,12,6,2)
	encabezado.Cell(1,2).SetOffsetSize(0,0,0,12,6,2)
	encabezado.Cell(1,3).SetOffsetSize(0,0,0,12,6,2)
	encabezado.Cell(1,4).SetOffsetSize(0,0,0,12,6,2)
	encabezado.Cell(1,5).SetOffsetSize(0,0,0,12,6,2)
	encabezado.Cell(1,6).SetOffsetSize(0,0,0,12,6,2)
	
	Dim i As Int
	
	For i= 1 To 6 
		encabezado.Cell(1,i).SetFixedHeight(75, False)
	Next
	
	Dim titulo1 As ABMLabel 
	titulo1.Initialize(page,"titulo1","OBJETIVOS A SER EVALUADOS",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,1).AddComponent(titulo1)
	
	Dim titulo2 As ABMLabel
	titulo2.Initialize(page,"titulo2","PUNTOS %",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,2).AddComponent(titulo2)
	Dim titulo3 As ABMLabel
	titulo3.Initialize(page,"titulo3","¿COMO DEBEN SER MEDIDOS? (Data cuantitativa)",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,3).AddComponent(titulo3)
	Dim titulo4 As ABMLabel
	titulo4.Initialize(page,"titulo4","EVALUACIÓN PROPIA DE DESMPEÑO",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,4).AddComponent(titulo4)
	Dim titulo5 As ABMLabel
	titulo5.Initialize(page,"titulo5","(Supervisor) INGRESE LOS COMENTARIOS DE LOS RESULTADOS",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,5).AddComponent(titulo5)
	Dim titulo6 As ABMLabel
	titulo6.Initialize(page,"titulo6","LETRA POR ESCALA",ABM.SIZE_H6,False,"")
	encabezado.Cell(1,6).AddComponent(titulo6)
	
	page.Cell(6,1).AddComponent(encabezado)
	
	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition
End Sub

Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub

Sub cmbPuesto_Clicked(itemId As String)
	'Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	'Dim cmby As ABMCombo=page.Component("cmbDepartamento")
	Dim cmbz As ABMCombo=page.Component("cmbempleados")
	Dim query As String
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	page.Pause
	sql1.Initialize(Main.strDrv,"jdbc:sqlserver://34.193.69.179:1433;databaseName=master;user=karismapass;password=SuiteHC$;")
	query ="Select * FROM OPENQUERY(GIN, 'select * from "&ws.Session.GetAttribute("DBUse")&".dbo.vw_lgen_altasybajas where Pkey_Pos_Key=''"&itemId&"''')"
	rst=sql1.ExecQuery(query)
	pos=0
	'cmbempleados.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbz.AddItem( rst.GetString2(0),rst.GetString2(1),BuildSimpleItem("P"& pos,"",rst.GetString2(1)))
		pos=pos+1
	Loop
	rst.close
	page.resume
	cmbz.Refresh
End Sub

Sub cmbDepartamento_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbPuesto")
	Dim cmby As ABMCombo=page.Component("cmbPuesto")
	cmby.Clear
	cmby.Refresh
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select * from RH_Positons where Pos_Dep_Key='" & itemId & "' order by 3")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem(rst.GetString2(0),rst.GetString2(2),BuildSimpleItem("P"& pos,"",rst.GetString2(2)))
		pos=pos+1
	Loop
	page.Resume
	rst.Close
	cmbx.Refresh
End Sub

Sub cmbSubDivision_Clicked(itemId As String)
	Dim cmbx As ABMCombo=page.Component("cmbLocation")
	Dim c1 As ABMCombo=page.Component("cmbSubDivision")
	Dim id1 As String=c1.GetActiveItemId
	Dim lb1 As ABMLabel=c1.GetComponent(id1)
	Dim sql1 As SQL
	Dim rst As ResultSet
	Dim pos As Int=0
	sql1.Initialize("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://34.193.69.179:1433;databaseName=karismaHC;user=karismapass;password=SuiteHC$;")
	rst=sql1.ExecQuery("Select distinct nom_propiedad from RH_SubDivision where nom_pais='" & lb1.Text & "'")
	cmbx.AddItem("P","",BuildSimpleItem("P","",""))
	Do While rst.NextRow
		cmbx.AddItem("P"& pos ,rst.GetString2(0),BuildSimpleItem("P"& pos,"",rst.GetString2(0)))
		pos=pos+1
	Loop
	rst.Close
	cmbx.Refresh
		
End Sub

Sub open_Clicked(Target As String)
	
	Dim open As ABMButton = page.Component("open")
	Dim oi As Int
	For oi = 1 To 4
		o(oi) = 0
	Next
	

		Dim valores(), valores2() As Object
		Dim pos As Int=0
		Dim idobj As Int
		Dim nomobj, nomorigobj As String
		Dim porobj, portotfocus As Double
		
		
	Dim sqlt, sqlt2 As String
	
	Dim eval, selfeval As String
	

	
	Dim C1 As ABMCombo=page.Component("cmbDivision")
	Dim T1 As String=C1.GetActiveItemId
	Dim L1 As ABMLabel=C1.GetComponent(T1)
	
	'	Dim C2 As ABMCombo=page.Component("cmbSubDivision")
	'	Dim T2 As String=C2.GetActiveItemId
	'	Dim L2 As ABMLabel=C2.GetComponent(T2)
	'
	
	Dim C3 As ABMCombo=page.Component("cmbLocation")
	Dim T3 As String=C3.GetActiveItemId
	Dim L3 As ABMLabel=C3.GetComponent(T3)
	
'	Dim C4 As ABMCombo=page.Component("cmbDepartamento")
'	Dim T4 As String=C4.GetActiveItemId
'	Dim L4 As ABMLabel=C4.GetComponent(T4)
	
	Dim C5 As ABMCombo=page.Component("cmbPuesto")
	Dim T5 As String=C5.GetActiveItemId
	Dim L5 As ABMLabel=C5.GetComponent(T5)
	
	Dim C6 As ABMCombo=page.Component("year")
	Dim T6 As String=C6.GetActiveItemId
	Dim L6 As ABMLabel=C6.GetComponent(T6)

	Dim R1 As ABMRadioGroup = page.Component("rbgroup")
	Dim periodo As Int = R1.GetActive 
	Dim P1 As String = periodo
	
	Dim C7 As ABMCombo=page.Component("cmbempleados")
	Dim T7 As String=C7.GetActiveItemId
	Dim L7 As ABMLabel=C7.GetComponent(T7)
	
	Dim C8 As ABMCombo=page.Component("cmbsupervisores")
	Dim T8 As String=C8.GetActiveItemId
	Dim L8 As ABMLabel=C8.GetComponent(T8)
		
	Dim j,z As Int = 1
		
	'
	
If open.Tag="open" Then	
		page.pause
		
		page.Cell(7,1).RemoveAllComponents
		Dim cuerpo As ABMContainer
	
		For i= 1 To enfoques.Size
			cuerpo.Initialize(page,"cuerpo","")
			cuerpo.AddRows(1,True,"").AddCells12MP(6,0,0,10,10,"cuerpocell")
			cuerpo.AddRows(1,True,"").AddCells12MP(1,0,0,0,0,"")
		Next
		
		
		cuerpo.BuildGrid
	
		page.Cell(7,1).AddComponent(cuerpo)
	
	
	z=1
	For i=1 To enfoques.Size * 2 Step 2
		
			Dim container As ABMContainer
			container.Initialize(page,"containerobj"&z,"") 
			'container.AddRows(1,True,"").AddCells12(1,"titulo1")
			'container.AddRows(1,True,"").AddCells12(1,"")
			'container.BuildGrid
					
			Dim container2 As ABMContainer
			container2.Initialize(page,"containermeasure"&z,"") 
			'container.AddRows(1,True,"").AddCells12(1,"titulo1")
			'container.AddRows(1,True,"").AddCells12(1,"")
			'container2.BuildGrid

			sqlt="Select * from RH_ObjEval where User_Id='"&T7&"' and User_Id_Eval='"&T8&"' and num_year="&L6.Text&" and num_period= "& P1 & " and id_focus='"&z&"'"
	
			rst=sql1.ExecQuery(sqlt)
								
			j=1
			' MIENTRAS PARA RECORRER OBJETIVOS DE UN FOCUS
			Do While rst.NextRow
				
				open.tag = "save"
				open.Text= "Save"
				open.Refresh
									
				sqlt2 = "Select * from RH_Objetive where id_objetive='"&rst.GetInt2(3)&"'"
				rst2=sql1.ExecQuery(sqlt2)
				
				
				' TRAER LOS DATOS DE ESE OBJETIVO
				Do While rst2.NextRow
					nomorigobj = rst2.GetString2(1)
					idobj = rst2.GetInt2(0)
				Loop	
				rst2.Close
				
				nomobj = rst.GetString("nom_objetive")
				
				container.AddRows(1,True,"").AddCells12(1,"")
				container.BuildGrid
				container.Cell(j,1).AddComponent(BuildSimpleItem(i&rst.GetInt2(3),"",nomobj&" Valor:"&(Round(rst.GetDouble2(5)*100))&"%"))
				
				container2.AddRows(1,True,"").AddCells12(1,"")
				container2.BuildGrid
				container2.Cell(j,1).AddComponent(BuildSimpleItem(i&rst.GetInt2(3),"",rst.GetString2(11)))
				
				
				porobj= rst.GetDouble2(9)
				eval =  rst.GetString2(7)
				selfeval = rst.GetString2(6)
				portotfocus = rst.GetString("por_pointstotfocus")
				
				j = j + 1
				
				o(z) = o(z) + 1
				
			Loop
								
			rst.Close
			
			cuerpo.Cell(i,1).Addcomponent(container)
			
			cuerpo.Cell(i,2).AddComponent( BuildSimpleItem("P"&z,"", Round(porobj*100)&"%" ))
 		
			cuerpo.Cell(i,3).AddComponent(container2)
			
			cuerpo.Cell(i,4).AddComponent(BuildSimpleinput("S"&z,ABM.INPUT_TEXT,selfeval))
			
			cuerpo.Cell(i,5).AddComponent(BuildSimpleinput("E"&z,ABM.INPUT_TEXT,eval))
			
			cuerpo.Cell(i,1).SetOffsetSize(0,0,0,12,6,2)
			cuerpo.Cell(i,2).SetOffsetSize(0,0,0,12,6,2)
			cuerpo.Cell(i,3).SetOffsetSize(0,0,0,12,6,2)
			cuerpo.Cell(i,4).SetOffsetSize(0,0,0,12,6,2)
			cuerpo.Cell(i,5).SetOffsetSize(0,0,0,12,6,2)
			cuerpo.Cell(i,6).SetOffsetSize(0,0,0,12,6,2)
			
			Dim divend As ABMDivider
			divend.Initialize(page, "divend", "divider")
			cuerpo.Cell(i+1,1).AddComponent(divend)
		
						
			Dim combo As ABMCombo
			combo.Initialize(page,"combo"&z,"Calificaciones",100,"combo")
		
			Dim x As Int 
		
			For x = 0 To calif.Size - 1
			     combo.AddItem("C"&z&x,calif.Get(x), BuildSimpleItem("C"&i&x&calif.Get(x), "", "{NBSP}{NBSP}"&calif.Get(x))) ' PARA PODER VER EL VALOR DEL COMBO
				If portotfocus = califval(x) Then
					combo.SetActiveItemId("C"&z&x)
				End If
					
					
			Next 
				
			cuerpo.Cell(i,6).AddComponent(combo)
									
			z=z+1
	Next
	

	 page.resume
	 page.Refresh
	 combo.Refresh
	
		
	Else
		
		
		If open.Tag="save" Then
			Dim cuerpo As ABMContainer = page.Component("cuerpo")
			page.pause	
				z=1			
				For i=1 To enfoques.Size * 2 Step 2
					
								
					Dim evalinp As ABMInput = cuerpo.Cell(i,5).Component("E"&z)
					Dim test As String = evalinp.Text
					
				
				    Dim selfevalinp As ABMInput  = cuerpo.Cell(i,4).Component("S"&z)
					
					Dim puntos As ABMCombo = cuerpo.Cell(i,6).component("combo"&z)
					Dim puntosid As String = puntos.GetActiveItemId
					Dim valpuntosid As ABMLabel=puntos.GetComponent(puntosid)
					
					
					Dim puntosfocus As Double = califval(puntosid.SubString(2))
					
					
					
					
					sqlt="UPDATE [karismaHC].[dbo].RH_ObjEval set por_pointstotfocus="&puntosfocus&" ,nom_selfeval = '"&selfevalinp.Text&"' , nom_comment = '"&evalinp.Text&"' where User_Id='"&T7&"' and User_Id_Eval='"&T8&"' and num_year="&L6.Text&" and num_period= "& P1 & " and id_focus='"&z&"'"
					sql1.ExecNonQuery(sqlt)
					
					z= z+1
				Next
		
		
				open.tag = "open"
				open.Text= "Open"
				open.Refresh
			
			
			Log ("FINALIZANDO GUARDADO")
		
		
			cuerpo.ClearGrid
			
		    page.Refresh
			'combo.Refresh
			
			page.resume
			page.ShowToast("toast1", "toastgreen", "GUARDADO FINALIZADO", 5000)
			ABMShared.NavigateToPage(page.ws, page.GetPageID, "../objetivosyresultados/")
		End If
			
	
	
	End If	
	
	
	
	
	
	
	Return
	

	
End Sub


Sub inputp1_Changed(value As String)
	
	Dim x As String = value
	
	
End Sub

Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub

Sub BuildSimpleItem(id As String, icon As String, Title As String) As ABMLabel
	Dim lbl As ABMLabel
	If icon <> "" Then
		lbl.Initialize(page, id, Title, ABM.SIZE_SMALL, True, "header")
	Else
		lbl.Initialize(page, id, Title, ABM.SIZE_SMALL, True, "")
	End If
	lbl.VerticalAlign = True
	lbl.IconName = icon
	Return lbl
End Sub

Sub BuildSimpleinput(id As String, tipo As String , text As String) As ABMInput
	Dim inp As ABMInput
	
	
'	If icon <> "" Then
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "header")
'	Else
'		lbl.Initialize(page, id, Title, ABM.SIZE_H6, True, "")
'	End If
	
	inp.Initialize(page, id, tipo, "", True, "")
	inp.Text = text
	
	
	Return inp
	'lbl.VerticalAlign = True
	'lbl.IconName = icon
	
	

End Sub

Sub buildcombo(id As String, icon As String, Title As String, inf As List) As ABMCombo
	
	Dim combo1 As ABMCombo
	combo1.Initialize(page,id,Title,100,"")
	
	Dim i As Int 
			
	If icon <> "" Then
		combo1.IconName = icon
	End If
	
	For i = 0 To inf.Size - 1
		'combo1.AddItem(i,inf.Get(i), BuildSimpleItem(i, icon, "{NBSP}{NBSP}"&inf.Get(i)))
	     combo1.AddItem(i,inf.Get(i), BuildSimpleItem(inf.Get(i), icon, "{NBSP}{NBSP}"&inf.Get(i))) ' PARA PODER VER EL VALOR DEL COMBO
	Next 
	
	'page.Refresh
	'combo1.Refresh	   
	Return combo1
	

End Sub

Sub Mensajes() As ABMModalSheet
	Dim myModal As ABMModalSheet
	myModal.Initialize(page, "fixedsheet", True, False, "")
	myModal.Content.UseTheme("")
	myModal.Footer.UseTheme("msgpie")
	myModal.IsDismissible = False
	myModal.Content.AddRows(1,True, "").AddCells12(1,"")	
	myModal.Content.BuildGrid
	Dim Message As String = $"Esto es un Dummy {BR}
	Es un ejemplo de como se veria las pantallas{BR}"$
	Dim lbl1 As ABMLabel
	lbl1.Initialize(page, "contlbl1", Message,ABM.SIZE_H1, False, "")
	myModal.Content.Cell(1,1).AddComponent(lbl1)
	
	myModal.Footer.AddRowsM(1,True,0,0, "").AddCellsOS(1,9,9,9,3,3,3,"")
	myModal.Footer.BuildGrid 
	Dim msbtn3 As ABMButton
	msbtn3.InitializeFlat(page, "msbtn3", "", "", "Cerrar", "transparent")
	myModal.Footer.Cell(1,1).AddComponent(msbtn3)	
	Return myModal
End Sub

Sub Page_Ready()
	Log("Lista objetivosyresultados")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub

