﻿Type=Class
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private ws As WebSocket 'ignore
	' will hold our page information
	Public page As ABMPage
	' page theme
	Private theme As ABMTheme
	' to access the constants
	Private ABM As ABMaterial 'ignore	
	' name of the page, must be the same as the class name (case sensitive!)
	Public Name As String = "altaobjetivo"  '<-------------------------------------------------------- IMPORTANT
	' will hold the unique browsers window id
	Private ABMPageId As String = "15"
	' your own variables		
	Dim myToastId As Int = 1
	
	Dim SLF,SLC,SLP,SLR As Int
	SLF=0
	SLC=0
	SLP=0
	SLR=0
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	' build the local structure IMPORTANT!
	BuildPage
End Sub

Private Sub WebSocket_Connected (WebSocket1 As WebSocket)
	'----------------------MODIFICATION-------------------------------	
	Log("Connected")
	ws = WebSocket1		
	ABMPageId = ABM.GetPageID(page, Name,ws)
	Dim session As HttpSession = ABM.GetSession(ws, ABMShared.SessionMaxInactiveIntervalSeconds)	
	
	If ABMShared.NeedsAuthorization Then
		If session.GetAttribute2("IsAuthorized", "") = "" Then
			ABMShared.NavigateToPage(ws, ABMPageId, "../")
			Return
		End If
	End If		
	
	ABM.UpdateFromCache(Me, ABMShared.CachedPages, ABMPageId, ws)
	If page.ComesFromPageCache Then
    	' when we have a page that is cached it doesn't matter if it comes or not from a new connection we serve the cached version.
		Log("Comes from cache")
    	page.Refresh
    	page.FinishedLoading
	Else
    	If page.WebsocketReconnected Then
			Log("Websocket reconnected")
        	' when we have a client that doesn't have the page in cache and it's websocket reconnected and also it's session is new - basically when the client had internet problems and it's session (and also cache) expired before he reconnected so the user has content in the browser but we don't have any on the server. So we need to reload the page.
        	' when a client that doesn't have the page in cache and it's websocket reconnected but it's session is not new - when the client had internet problems and when he reconnected it's session was valid but he had no cache for this page we need to reload the page as the user browser has content, reconnected but we have no content in cache
        	ABMShared.NavigateToPage (ws, ABMPageId, "./" & page.PageHTMLName)
    	Else
        	' when the client did not reconnected it doesn't matter if the session was new or not because this is the websockets first connection so no dynamic content in the browser ... we are going to serve the dynamic content...
        	Log("Websocket first connection")
			page.Prepare
        	ConnectPage
    	End If
	End If
	Log(ABMPageId)	
	'----------------------MODIFICATION-------------------------------	
End Sub

Private Sub WebSocket_Disconnected
	Log("Disconnected")	
End Sub

Sub Page_ParseEvent(Params As Map) 
	Dim eventName As String = Params.Get("eventname")
	Dim eventParams() As String = Regex.Split(",",Params.Get("eventparams"))
	If eventName = "beforeunload" Then
		Log("preparing for url refresh")	
		ABM.RemoveMeFromCache(ABMShared.CachedPages, ABMPageId)	
		Return
	End If
	If SubExists(Me, eventName) Then
		Params.Remove("eventname")
		Params.Remove("eventparams")
		Select Case Params.Size
			Case 0
				CallSub(Me, eventName)
			Case 1
				CallSub2(Me, eventName, Params.Get(eventParams(0)))					
			Case 2
				If Params.get(eventParams(0)) = "abmistable" Then
					Dim PassedTables As List = ABM.ProcessTablesFromTargetName(Params.get(eventParams(1)))
					CallSub2(Me, eventName, PassedTables)
				Else
					CallSub3(Me, eventName, Params.Get(eventParams(0)), Params.Get(eventParams(1)))
				End If
			Case Else
				' cannot be called directly, to many param
				CallSub2(Me, eventName, Params)				
		End Select
	End If
		End Sub

public Sub BuildTheme()
	theme.Initialize("pagetheme")
	theme.AddABMTheme(ABMShared.MyTheme)
	theme.AddContainerTheme("conte")
	theme.Container("conte").ZDepth=ABM.ZDEPTH_3
	theme.AddDividerTheme("div1")
	theme.Divider("div1").ForeColor=ABM.COLOR_BLUE
	theme.Divider("div1").ForeColorIntensity=ABM.INTENSITY_LIGHTEN4
	theme.AddImageSliderTheme("sl1")
	theme.ImageSlider("sl1").Height=200
	theme.ImageSlider("sl1").Indicators=False
	theme.ImageSlider("sl1").FullWidth=True
	theme.ImageSlider("sl1").ZDepth=ABM.ZDEPTH_3
	
	
		'Combos
	theme.AddComboTheme("combo")
	theme.Combo("combo").ZDepth=ABM.ZDEPTH_3
		'Radio
	theme.AddRadioGroupTheme("radio")
	theme.RadioGroup("radio").WithGab = True
	
		'label
	theme.AddlabelTheme("label1")
	theme.Label("label1").ForeColor = ABM.COLOR_WHITE	
	theme.Label("label1").BackColor = ABM.COLOR_BROWN
    theme.Label("label1").BackColorIntensity = ABM.INTENSITY_DARKEN4
	theme.Label("label1").Align = ABM.TEXTALIGN_CENTER
	theme.Label("label1").ZDepth = ABM.ZDEPTH_1
	
	'label gral 100%
	theme.AddLabelTheme("gral100")
	theme.Label("gral100").ForeColor=ABM.COLOR_GREEN
'	theme.Label("gral100").BackColorIntensity=ABM.INTENSITY_LIGHTEN5
	
	'label sin 100
	theme.AddLabelTheme("gral")
	theme.Label("gral").ForeColor=ABM.COLOR_BLACK
	
	'tema de celda titulo
	theme.AddInputTheme("input1")
	theme.Input("input1").FocusForeColor = ABM.COLOR_WHITE
	theme.Input("input1").InputColor = ABM.COLOR_black
	theme.Input("input1").ValidColor = ABM.COLOR_WHITE
	theme.Input("input1").InvalidColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").AutoCompleteHoverBackColor = ABM.COLOR_WHITE 
	theme.Input("input1").ZDepth = ABM.ZDEPTH_1
End Sub

public Sub BuildPage()
	' initialize the theme
	BuildTheme
	
	' initialize this page using our theme
	page.InitializeWithTheme(Name, "/ws/" & ABMShared.AppName & "/" & Name, False, ABMShared.SessionMaxInactiveIntervalSeconds, theme)
	page.ShowLoader=True
	page.PageHTMLName = "index.html"
	page.PageTitle = ""
	page.PageDescription = ""
	page.PageKeywords = ""
	page.PageSiteMapPriority = ""
	page.PageSiteMapFrequency = ABM.SITEMAP_FREQ_YEARLY
	
	page.ShowConnectedIndicator = True
		
	' Añadir Navigation Bar
	ABMShared.BuildNavigationBar(page,"POC","../images/klogo3.png","","POC","")
	page.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	page.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,6,6,0,0,0,0,"").AddCellsOSMP(1,0,1,1,12,5,5,0,0,0,0,"")
	page.AddRows(4,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	
	page.BuildGrid 'IMPORTANT once you loaded the complete grid AND before you start adding components
		
End Sub

public Sub ConnectPage()

	Dim cnt As ABMContainer
	cnt.Initialize(page,"cnt","")
	cnt.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,10,10,10,5,0,10,0,"").AddCellsOSMPV(1,0,0,0,1,1,1,10,0,0,0,ABM.VISIBILITY_HIDE_ON_SMALL_ONLY,"")
	
	cnt.BuildGrid
	page.Cell(1,1).MarginTop="1%"
	page.Cell(1,1).AddComponent(cnt)

	Dim lblt1 As ABMLabel
	'	If idmX = "ING" Then
	'		lblt1.Initialize(page,"iblt1","{C:#795548}Welcome to the evaluation system{/C}",ABM.SIZE_H5,False,"justifybr")
	'	Else
	lblt1.Initialize(page,"iblt1","{C:#795548}Asignación de objetivos POC{/C}",ABM.SIZE_H5,False,"justifybr")
	'	End If
	'
	'lblt1.IsFlowText=True
	cnt.Cell(1,1).AddComponent(lblt1)

	Dim imglogo As ABMImage
	imglogo.Initialize(page,"iml","../images/POC.png",3)
	imglogo.IsResponsive=True
	cnt.Cell(1,2).AddComponent(imglogo)
	cnt.Cell(1,2).SetFixedHeight(50,False)

	Dim cnt1 As ABMContainer
	cnt1.Initialize(page,"cnt1","cnt")
	cnt1.AddRows(8,True,"").AddCellsOSMP(1,0,0,0,12,12,12,0,0,0,0,"")
	'cnt1.AddRows(2,True,"").AddCellsOSMP(1,0,0,0,12,5,5,0,0,0,0,"").AddCellsOSMP(1,0,1,2,12,5,5,0,0,0,0,"")

	cnt1.BuildGrid
	page.Cell(2,1).MarginTop="1%"
	page.Cell(2,1).AddComponent(cnt1)
	
	cnt1.Cell(1,1).UseTheme("celda")
	cnt1.Cell(1,1).MarginTop="1%"
	cnt1.Cell(1,1).AddComponent(Utils.TituloMediano("EMPLEADO",page))

	cnt1.Cell(2,1).UseTheme("celda")
	cnt1.Cell(2,1).MarginTop="1%"
	cnt1.Cell(2,1).AddComponent(Utils.Titulogrande("RICARDO TORRES MENCHACA",page))
	
	cnt1.Cell(3,1).UseTheme("celda")
	cnt1.Cell(3,1).MarginTop="1%"
	cnt1.Cell(3,1).AddComponent(Utils.TituloMediano("PUESTO",page))
	
	cnt1.Cell(4,1).UseTheme("celda")
	cnt1.Cell(4,1).MarginTop="1%"
	cnt1.Cell(4,1).AddComponent(Utils.Titulogrande("GERENTE SISTEMAS",page))
	
	
	cnt1.Cell(5,1).UseTheme("celda")
	cnt1.Cell(5,1).MarginTop="1%"
	cnt1.Cell(5,1).AddComponent(Utils.TituloMediano("DEPARTAMENTO",page))
	
	cnt1.Cell(6,1).UseTheme("celda")
	cnt1.Cell(6,1).MarginTop="1%"
	cnt1.Cell(6,1).AddComponent(Utils.TituloGrande("SISTEMAS",page))



	
	Dim cnt1enf As ABMContainer
	cnt1enf.Initialize(page,"cnt1enf","cnt")
	cnt1enf.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,12,12,12,10,10,0,0,"")
	cnt1enf.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,10,10,10,0,0,0,0,"").AddCellsOSMP(1,0,1,1,1,1,1,0,0,0,0,"")
	cnt1enf.AddRows(5,True,"").AddCellsOSMP(1,0,0,0,6,6,6,0,0,0,0,"").AddCellsOSMP(1,0,0,0,4,4,4,6,0,0,0,"").AddCellsOSMP(1,1,1,1,1,1,1,0,0,0,0,"")
	cnt1enf.BuildGrid
	page.Cell(2,2).MarginTop="1%"
	page.Cell(2,2).AddComponent(cnt1enf)
	
	cnt1enf.Cell(1,1).UseTheme("celda")
	cnt1enf.Cell(1,1).MarginTop="1%"
	cnt1enf.Cell(1,1).AddComponent(Utils.TituloGrande("Enfoques",page))

	cnt1enf.Cell(2,1).UseTheme("celda")
	cnt1enf.Cell(2,1).MarginTop="1%"
	cnt1enf.Cell(2,1).AddComponent(Utils.TituloMediano("Total de enfoques",page))

	Dim lblGral As ABMLabel
	lblGral.Initialize(page,"lblgral","0%",ABM.SIZE_A,False,"")
	cnt1enf.Cell(2,2).UseTheme("celda")
	cnt1enf.Cell(2,2).MarginTop="1%"
	cnt1enf.Cell(2,2).AddComponent(lblGral)

	gralenfoques(cnt1enf,3,"Enfoque financiero","A",page)
	gralenfoques(cnt1enf,4,"Enfoque cliente/huesped","B",page)
	gralenfoques(cnt1enf,5,"Enfoque a procesos","C",page)
	gralenfoques(cnt1enf,6,"Enfoque al personal","D",page)
	
	cargadeobj(3,"ENFOQUE FINANCIERO","fin")
	cargadeobj(4,"ENFOQUE CLIENTE/HUESPED","cli")
	cargadeobj(5,"ENFOQUE A PROCESOS","pro")
	cargadeobj(6,"ENFOQUE AL PERSONAL","per")


	
'	creaflotante
	
	' refresh the page
	page.Refresh
	' Tell the browser we finished loading
	page.FinishedLoading
	' restoring the navigation bar position
	page.RestoreNavigationBarPosition

	page.ws.Eval(Utils.Diseño,Null)
	
End Sub

Sub sumaGral As Boolean
	Dim cntfin As ABMContainer=page.Component("cnt1enf")
	Dim lblporA As ABMLabel=cntfin.Component("porA")
	Dim lblporB As ABMLabel=cntfin.Component("porB")
	Dim lblporC As ABMLabel=cntfin.Component("porC")
	Dim lblporD As ABMLabel=cntfin.Component("porD")
	Dim lblgral As ABMLabel=cntfin.Component("lblgral")
	'validar
	If (lblporA.Tag+lblporB.Tag+lblporC.Tag+lblporD.Tag)>100.1 Then
		page.ShowToast(myToastId+1,"","No puede sobrepasar el 100%",3000)
		lblgral.UseTheme("gral")
		Return False
	else If (lblporA.Tag+lblporB.Tag+lblporC.Tag+lblporD.Tag)=100 Then
		lblgral.Text=(lblporA.Tag+lblporB.Tag+lblporC.Tag+lblporD.Tag) & "%"
		lblgral.UseTheme("gral100")
		lblgral.Refresh
		Return True
	Else
		lblgral.Text=(lblporA.Tag+lblporB.Tag+lblporC.Tag+lblporD.Tag) & "%"
		lblgral.UseTheme("gral")
		lblgral.Refresh
		Return True
	End If

End Sub
Sub sliderA_Changed(value As String)
	Dim cntx As ABMContainer=page.Component("cnt1enf")
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim lblsv As ABMLabel=cntfin.Component("lblsvfin")
	Dim lblpor As ABMLabel=cntx.Component("porA")
	Dim cntS As ABMContainer=cntx.Component("cntA")
	Dim slider As ABMSlider=cntS.Component("sliderA")
	lblpor.tag=value
	lblpor.Refresh
	If 	sumaGral=False Then
		lblpor.Text="0%"
		lblpor.tag=0
		lblsv.Text="0%"
		lblsv.Tag=0
		slider.SetValue(0)
		SLF=0
		lblpor.Refresh
		slider.Refresh
		sumaGral		
	Else
		lblpor.Text=value & "%"
		lblpor.tag=value
		lblsv.Text=value & "%"
		lblsv.Tag=value
		lblsv.Refresh
		lblpor.Refresh
		SLF=value
		sumaGral
	End If
End Sub
Sub btnsvfin_Clicked(Target As String)
	Log("aqui")
End Sub 
Sub sliderB_Changed(value As String)
	Dim cntx As ABMContainer=page.Component("cnt1enf")
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim lblsv As ABMLabel=cntcli.Component("lblsvcli")
	Dim lblpor As ABMLabel=cntx.Component("porB")
	Dim cntS As ABMContainer=cntx.Component("cntB")
	Dim slider As ABMSlider=cntS.Component("sliderB")
	lblpor.tag=value
	lblpor.Refresh
	If 	sumaGral=False Then
		lblpor.Text="0%"
		lblpor.tag=0
		lblsv.Text="0%"
		lblsv.Tag=0
		slider.SetValue(0)
		SLC=0
		lblpor.Refresh
		slider.Refresh
		sumaGral
	Else
		lblpor.Text=value & "%"
		lblpor.tag=value
		lblsv.Text=value & "%"
		lblsv.Tag=value
		lblsv.Refresh
		lblpor.Refresh
		SLC=value
	End If
End Sub
Sub sliderC_Changed(value As String)
	Dim cntx As ABMContainer=page.Component("cnt1enf")
	Dim cntfin As ABMContainer=page.Component("cntpro")
	Dim lblsv As ABMLabel=cntfin.Component("lblsvpro")
	Dim lblpor As ABMLabel=cntx.Component("porC")
	Dim cntS As ABMContainer=cntx.Component("cntC")
	Dim slider As ABMSlider=cntS.Component("sliderC")
	lblpor.tag=value
	lblpor.Refresh
	If 	sumaGral=False Then
		lblpor.Text="0%"
		lblpor.tag=0
		lblsv.Text="0%"
		lblsv.Tag=0
		slider.SetValue(0)
		SLP=0
		lblpor.Refresh
		slider.Refresh
		sumaGral
	Else
		lblpor.Text=value & "%"
		lblpor.tag=value
		lblsv.Text=value & "%"
		lblsv.Tag=value
		lblsv.Refresh
		lblpor.Refresh
		SLP=value
	End If
End Sub
Sub sliderD_Changed(value As String)
	Dim cntx As ABMContainer=page.Component("cnt1enf")
	Dim cntfin As ABMContainer=page.Component("cntper")
	Dim lblsv As ABMLabel=cntfin.Component("lblsvper")
	Dim lblpor As ABMLabel=cntx.Component("porD")
	Dim cntS As ABMContainer=cntx.Component("cntD")
	Dim slider As ABMSlider=cntS.Component("sliderD")
	lblpor.tag=value
	lblpor.Refresh
	If 	sumaGral=False Then
		lblpor.Text="0%"
		lblpor.tag=0
		lblsv.Text="0%"
		lblsv.Tag=0
		slider.SetValue(0)
		SLR=0
		lblpor.Refresh
		slider.Refresh
		sumaGral
	Else
		lblpor.Text=value & "%"
		lblpor.tag=value
		lblsv.Text=value & "%"
		lblsv.Tag=value
		lblsv.Refresh
		lblpor.Refresh
		SLR=value
	End If
End Sub


Sub gralenfoques(cnt1enf As ABMContainer,rx As Int,Tituloenf As String,postFix As String,pagina As ABMPage)
	
	cnt1enf.Cell(rx,1).UseTheme("celda")
	cnt1enf.Cell(rx,1).MarginTop="1%"
	cnt1enf.Cell(rx,1).SetFixedHeight(33,False)
	cnt1enf.Cell(rx,1).AddComponent(Utils.TituloMediano(Tituloenf,page))
	Dim cont As ABMContainer
	cont.Initialize(page,"cnt"& postFix,"")
	cont.AddRows(1,True,"").AddCells12(1,"")
	cont.BuildGrid
	cont.Cell(1,1).SetFixedHeight(48,False)
	cnt1enf.Cell(rx,2).UseTheme("celdasl")
	cnt1enf.Cell(rx,2).AddComponent(cont)
	Dim slider As ABMSlider
	slider.Initialize(page, "slider" & postFix, 0, 0, 100, 1, "slider")
	slider.HandleToolTip = "wNumb({ decimals: 0 })"
	cont.Cell(1,1).MarginTop="5%"
	cont.Cell(1,1).MarginLeft="10%"
	cont.Cell(1,1).PaddingRight="20%"
	cont.Cell(1,1).SetFixedHeight(0,False)
	slider.Connect = ABM.SLIDER_CONNECT_UPPER
	cont.Cell(1,1).AddComponent(slider)
	'cont.Cell(1,1).AddArrayComponent(slider,"slv")
	Dim por As ABMLabel
	por.Initialize(page,"por"&postFix,"0 %",ABM.SIZE_A,False,"")
	por.Tag="0"
	cnt1enf.Cell(rx,3).UseTheme("celda")
	cnt1enf.Cell(rx,3).MarginTop="1%"
	cnt1enf.Cell(rx,3).SetFixedHeight(33,False)
	cnt1enf.Cell(rx,3).AddComponent(por)
End Sub



Sub cargadeobj (Rx As Int,TituloEnf As String,preFix As String)
	Dim cntfin As ABMContainer
	cntfin.Initialize(page,"cnt"&preFix,"cnt")
	cntfin.AddRows(1,True,"").AddCells12(1,"")
	cntfin.AddRows(1,True,"").AddCellsOSMP(1,0,0,0,5,5,5,0,0,0,0,"").AddCellsOSMP(1,1,1,1,1,1,1,0,0,0,0,"").AddCellsOSMP(1,1,1,1,2,2,2,0,0,0,0,"").AddCellsOSMP(1,1,1,1,1,1,1,0,0,0,0,"")
	cntfin.AddRows(9,True,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"").AddCellsOSMP(1,0,0,0,11,5,5,0,0,0,0,"").AddCellsOSMP(1,0,0,0,1,1,1,0,0,0,0,"").AddCellsOSMP(1,0,0,0,2,3,3,0,0,0,0,"").AddCellsOSMP(1,0,0,0,2,2,2,0,0,0,0,"")
	cntfin.BuildGrid
	page.Cell(Rx,1).SetFixedHeight(600,False)
	page.Cell(Rx,1).MarginTop="1%"
	page.Cell(Rx,1).AddComponent(cntfin)
	cntfin.Cell(1,1).UseTheme("celda")
	cntfin.Cell(1,1).MarginTop="1%"
	cntfin.Cell(1,1).AddComponent(Utils.TituloGrande(TituloEnf,page))
	cntfin.Cell(2,1).UseTheme("celda")
	cntfin.Cell(2,1).MarginTop="1%"
	cntfin.Cell(2,1).AddComponent(Utils.TituloMediano("Peso del Enfoque",page))
	Dim lblsv As ABMLabel
	lblsv.Initialize(page,"lblsv" & preFix ,"0%",ABM.SIZE_A,False,"")
	cntfin.Cell(2,2).UseTheme("celda")
	cntfin.Cell(2,2).MarginTop="1%"
	cntfin.Cell(2,2).AddComponent(lblsv)
	cntfin.Cell(2,3).UseTheme("celda")
	cntfin.Cell(2,3).MarginTop="1%"
	cntfin.Cell(2,3).AddComponent(Utils.TituloMediano("Suma de los objetivos",page))
	Dim lblso As ABMLabel
	lblso.Initialize(page,"lblso" & preFix ,"0 puntos",ABM.SIZE_A,False,"")
	cntfin.Cell(2,4).UseTheme("celda")
	cntfin.Cell(2,4).MarginTop="1%"
	cntfin.Cell(2,4).AddComponent(lblso)
	'titulos de datos
	cntfin.Cell(3,1).UseTheme("celda")
	cntfin.Cell(3,1).MarginTop="1%"
	cntfin.Cell(3,1).AddComponent(Utils.TituloMediano("#",page))
	cntfin.Cell(3,2).UseTheme("celda")
	cntfin.Cell(3,2).MarginTop="1%"
	cntfin.Cell(3,2).AddComponent(Utils.TituloMediano("Objetivo",page))
	
	cntfin.Cell(3,3).UseTheme("celda")
	cntfin.Cell(3,3).MarginTop="1%"
	cntfin.Cell(3,3).AddComponent(Utils.TituloMediano("Operador",page))
	
	cntfin.Cell(3,4).UseTheme("celda")
	cntfin.Cell(3,4).MarginTop="1%"
	cntfin.Cell(3,4).AddComponent(Utils.TituloMediano("Peso",page))
	cntfin.Cell(3,5).UseTheme("celda")
	cntfin.Cell(3,5).MarginTop="1%"
	cntfin.Cell(3,5).AddComponent(Utils.TituloMediano("Metrica",page))
	Dim Mapv As Map
	Mapv.Initialize
	Mapv.Put(">",">")
	Mapv.Put("<","<")
	Mapv.Put(">=",">=")
	Mapv.Put("<=","<=")
	Mapv.Put("=","=")

	For ob =4 To 8
		'---------enfoque financiero 1
		cntfin.Cell(ob,1).UseTheme("celda")
		cntfin.Cell(ob,1).SetFixedHeight(48,False)
		cntfin.Cell(ob,1).AddComponent(Utils.TituloMediano(ob-3,page))
		cntfin.Cell(ob,2).UseTheme("celda")
		cntfin.Cell(ob,2).AddComponent(Utils.InputTextoNw(page,"","txt"&preFix&ob,"-up"))
		'combo
		cntfin.Cell(ob,3).UseTheme("celda")
		cntfin.Cell(ob,3).AddComponent(Utils.Combosimple(page,"cmb"&preFix&ob,"",Mapv))
		Dim cntfin11 As ABMContainer
		cntfin11.Initialize(page,"cnt"& preFix & ob,"")
		cntfin11.AddRows(1,True,"").AddCells12(1,"")
		cntfin11.BuildGrid
		cntfin11.Cell(1,1).SetFixedHeight(48,False)
		cntfin.Cell(ob,4).UseTheme("celdasl")
		cntfin.Cell(ob,4).AddComponent(cntfin11)
		Dim slider11 As ABMSlider
		slider11.Initialize(page, "slider" & preFix & ob, 0, 0, 100, 1, "slider")
		slider11.HandleToolTip = "wNumb({ decimals: 0 })"
		cntfin11.Cell(1,1).MarginTop="5%"
		cntfin11.Cell(1,1).SetFixedHeight(10,False)
		slider11.Connect = ABM.SLIDER_CONNECT_UPPER
		cntfin11.Cell(1,1).AddComponent(slider11)
		cntfin.Cell(ob,5).UseTheme("celda")
		cntfin.Cell(ob,5).SetFixedHeight(48,False)
		cntfin.Cell(ob,5).AddComponent(Utils.InputTextoNw(page,"","txt2" & preFix & ob,"-de"))
	Next
	Dim btnsv As ABMButton
	btnsv.InitializeFlat(page,"btnsv"&preFix,"","","Guardar","biggreen")
	'cntfin.Cell(10,1).AddComponent(btnsv)
	cntfin.Cell(10,1).AddArrayComponent(btnsv, "btnsave")
	'-------
End Sub
Sub btnsave_Clicked(Target As String)
	Log("button " & Target)
	'validaciones,
	Dim prefix As String=Target.SubString(12)
	Dim cntb As ABMContainer=page.Component("cnt" & prefix)
	
	Dim lbl As ABMLabel=cntb.ComponentRowCell(1,1,"lbl2")
	Log(lbl.Text)
	
	For ob=4 To 8
		Dim txt1 As ABMInput = cntb.ComponentRowCell(ob,2,"txt"&prefix&ob&"-up")
		Log(txt1.Text)
		Dim cmb1 As ABMCombo=cntb.ComponentRowCell(ob,3,"cmb"&prefix&ob)
		Log(cmb1.GetActiveItemId)
		Dim cntsl As ABMContainer=cntb.ComponentRowCell(ob,4,"cnt"& prefix & ob)
		Dim sl1 As ABMSlider=cntsl.ComponentRowCell(1,1,"slider" & prefix & ob)
		Log("slider " & sl1.GetValue)
		
		Dim txt2 As ABMInput = cntb.ComponentRowCell(ob,5,"txt2"&prefix&ob&"-de")
		Log("Texto 2" & txt2.Text)
	Next
	
	
	
End Sub

Sub SumaSlFin As Boolean
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim lblsoX As ABMLabel=cntfin.Component("lblsofin")
	Dim cntfinX4 As ABMContainer=cntfin.Component("cntfin4")
	Dim sliderX4 As ABMSlider=cntfinX4.Component("sliderfin4")
	Dim cntfinX5 As ABMContainer=cntfin.Component("cntfin5")
	Dim sliderX5 As ABMSlider=cntfinX5.Component("sliderfin5")

	Dim cntfinX6 As ABMContainer=cntfin.Component("cntfin6")
	Dim sliderX6 As ABMSlider=cntfinX6.Component("sliderfin6")
	Dim cntfinX7 As ABMContainer=cntfin.Component("cntfin7")
	Dim sliderX7 As ABMSlider=cntfinX7.Component("sliderfin7")
	Dim cntfinX8 As ABMContainer=cntfin.Component("cntfin8")
	Dim sliderX8 As ABMSlider=cntfinX8.Component("sliderfin8")
	Dim suma As Int
	suma=sliderX4.GetValue+sliderX5.GetValue+sliderX6.GetValue+sliderX7.GetValue+sliderX8.GetValue
	If suma>SLF Then
		page.ShowToast(myToastId+1,"","Se paso",3000)
		Return False
	else if suma=SLF Then
		lblsoX.Text=suma&" puntos"
		lblsoX.UseTheme("gral100")
		lblsoX.refresh
		Return True
	Else
		lblsoX.Text=suma&" puntos"
		lblsoX.UseTheme("gral")
		lblsoX.refresh
		Return True
	End If
	
End Sub


Sub sliderfin4_Changed(value As String)
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim cntfinX As ABMContainer=cntfin.Component("cntfin4")
	Dim sliderX As ABMSlider=cntfinX.Component("sliderfin4")
	If SumaSlFin=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlFin
	End If
End Sub
Sub sliderfin5_Changed(value As String)
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim cntfinX As ABMContainer=cntfin.Component("cntfin5")
	Dim sliderX As ABMSlider=cntfinX.Component("sliderfin5")
	If SumaSlFin=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlFin
	End If
End Sub
Sub sliderfin6_Changed(value As String)
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim cntfinX As ABMContainer=cntfin.Component("cntfin6")
	Dim sliderX As ABMSlider=cntfinX.Component("sliderfin6")
	If SumaSlFin=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlFin
	End If
End Sub
Sub sliderfin7_Changed(value As String)
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim cntfinX As ABMContainer=cntfin.Component("cntfin7")
	Dim sliderX As ABMSlider=cntfinX.Component("sliderfin7")
	If SumaSlFin=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlFin
	End If
End Sub
Sub sliderfin8_Changed(value As String)
	Dim cntfin As ABMContainer=page.Component("cntfin")
	Dim cntfinX As ABMContainer=cntfin.Component("cntfin8")
	Dim sliderX As ABMSlider=cntfinX.Component("sliderfin8")
	If SumaSlFin=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlFin
	End If
End Sub


Sub SumaSlcli As Boolean
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim lblsoX As ABMLabel=cntcli.Component("lblsocli")
	Dim cntcliX4 As ABMContainer=cntcli.Component("cntcli4")
	Dim sliderX4 As ABMSlider=cntcliX4.Component("slidercli4")
	Dim cntcliX5 As ABMContainer=cntcli.Component("cntcli5")
	Dim sliderX5 As ABMSlider=cntcliX5.Component("slidercli5")

	Dim cntcliX6 As ABMContainer=cntcli.Component("cntcli6")
	Dim sliderX6 As ABMSlider=cntcliX6.Component("slidercli6")
	Dim cntcliX7 As ABMContainer=cntcli.Component("cntcli7")
	Dim sliderX7 As ABMSlider=cntcliX7.Component("slidercli7")
	Dim cntcliX8 As ABMContainer=cntcli.Component("cntcli8")
	Dim sliderX8 As ABMSlider=cntcliX8.Component("slidercli8")
	Dim suma As Int
	suma=sliderX4.GetValue+sliderX5.GetValue+sliderX6.GetValue+sliderX7.GetValue+sliderX8.GetValue
	If suma>SLC Then
		page.ShowToast(myToastId+1,"","Se paso",3000)
		Return False
	Else
		lblsoX.Text=suma&" puntos"
		lblsoX.refresh
		Return True
	End If
	
End Sub


Sub slidercli4_Changed(value As String)
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim cntcliX As ABMContainer=cntcli.Component("cntcli4")
	Dim sliderX As ABMSlider=cntcliX.Component("slidercli4")
	If SumaSlcli=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlcli
	End If
End Sub
Sub slidercli5_Changed(value As String)
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim cntcliX As ABMContainer=cntcli.Component("cntcli5")
	Dim sliderX As ABMSlider=cntcliX.Component("slidercli5")
	If SumaSlcli=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlcli
	End If
End Sub
Sub slidercli6_Changed(value As String)
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim cntcliX As ABMContainer=cntcli.Component("cntcli6")
	Dim sliderX As ABMSlider=cntcliX.Component("slidercli6")
	If SumaSlcli=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlcli
	End If
End Sub
Sub slidercli7_Changed(value As String)
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim cntcliX As ABMContainer=cntcli.Component("cntcli7")
	Dim sliderX As ABMSlider=cntcliX.Component("slidercli7")
	If SumaSlcli=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlcli
	End If
End Sub
Sub slidercli8_Changed(value As String)
	Dim cntcli As ABMContainer=page.Component("cntcli")
	Dim cntcliX As ABMContainer=cntcli.Component("cntcli8")
	Dim sliderX As ABMSlider=cntcliX.Component("slidercli8")
	If SumaSlcli=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlcli
	End If
End Sub

Sub SumaSlpro As Boolean
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim lblsoX As ABMLabel=cntpro.Component("lblsopro")
	Dim cntproX4 As ABMContainer=cntpro.Component("cntpro4")
	Dim sliderX4 As ABMSlider=cntproX4.Component("sliderpro4")
	Dim cntproX5 As ABMContainer=cntpro.Component("cntpro5")
	Dim sliderX5 As ABMSlider=cntproX5.Component("sliderpro5")

	Dim cntproX6 As ABMContainer=cntpro.Component("cntpro6")
	Dim sliderX6 As ABMSlider=cntproX6.Component("sliderpro6")
	Dim cntproX7 As ABMContainer=cntpro.Component("cntpro7")
	Dim sliderX7 As ABMSlider=cntproX7.Component("sliderpro7")
	Dim cntproX8 As ABMContainer=cntpro.Component("cntpro8")
	Dim sliderX8 As ABMSlider=cntproX8.Component("sliderpro8")
	Dim suma As Int
	suma=sliderX4.GetValue+sliderX5.GetValue+sliderX6.GetValue+sliderX7.GetValue+sliderX8.GetValue
	If suma>SLP Then
		page.ShowToast(myToastId+1,"","Se paso",3000)
		Return False
	Else
		lblsoX.Text=suma&" puntos"
		lblsoX.refresh
		Return True
	End If
	
End Sub


Sub sliderpro4_Changed(value As String)
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim cntproX As ABMContainer=cntpro.Component("cntpro4")
	Dim sliderX As ABMSlider=cntproX.Component("sliderpro4")
	If SumaSlpro=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlpro
	End If
End Sub
Sub sliderpro5_Changed(value As String)
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim cntproX As ABMContainer=cntpro.Component("cntpro5")
	Dim sliderX As ABMSlider=cntproX.Component("sliderpro5")
	If SumaSlpro=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlpro
	End If
End Sub
Sub sliderpro6_Changed(value As String)
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim cntproX As ABMContainer=cntpro.Component("cntpro6")
	Dim sliderX As ABMSlider=cntproX.Component("sliderpro6")
	If SumaSlpro=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlpro
	End If
End Sub
Sub sliderpro7_Changed(value As String)
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim cntproX As ABMContainer=cntpro.Component("cntpro7")
	Dim sliderX As ABMSlider=cntproX.Component("sliderpro7")
	If SumaSlpro=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlpro
	End If
End Sub
Sub sliderpro8_Changed(value As String)
	Dim cntpro As ABMContainer=page.Component("cntpro")
	Dim cntproX As ABMContainer=cntpro.Component("cntpro8")
	Dim sliderX As ABMSlider=cntproX.Component("sliderpro8")
	If SumaSlpro=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlpro
	End If
End Sub

Sub SumaSlper As Boolean
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim lblsoX As ABMLabel=cntper.Component("lblsoper")
	Dim cntperX4 As ABMContainer=cntper.Component("cntper4")
	Dim sliderX4 As ABMSlider=cntperX4.Component("sliderper4")
	Dim cntperX5 As ABMContainer=cntper.Component("cntper5")
	Dim sliderX5 As ABMSlider=cntperX5.Component("sliderper5")

	Dim cntperX6 As ABMContainer=cntper.Component("cntper6")
	Dim sliderX6 As ABMSlider=cntperX6.Component("sliderper6")
	Dim cntperX7 As ABMContainer=cntper.Component("cntper7")
	Dim sliderX7 As ABMSlider=cntperX7.Component("sliderper7")
	Dim cntperX8 As ABMContainer=cntper.Component("cntper8")
	Dim sliderX8 As ABMSlider=cntperX8.Component("sliderper8")
	Dim suma As Int
	suma=sliderX4.GetValue+sliderX5.GetValue+sliderX6.GetValue+sliderX7.GetValue+sliderX8.GetValue
	If suma>SLR Then
		page.ShowToast(myToastId+1,"","Se paso",3000)
		Return False
	Else
		lblsoX.Text=suma&" puntos"
		lblsoX.refresh
		Return True
	End If
	
End Sub


Sub sliderper4_Changed(value As String)
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim cntperX As ABMContainer=cntper.Component("cntper4")
	Dim sliderX As ABMSlider=cntperX.Component("sliderper4")
	If SumaSlper=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlper
	End If
End Sub
Sub sliderper5_Changed(value As String)
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim cntperX As ABMContainer=cntper.Component("cntper5")
	Dim sliderX As ABMSlider=cntperX.Component("sliderper5")
	If SumaSlper=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlper
	End If
End Sub
Sub sliderper6_Changed(value As String)
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim cntperX As ABMContainer=cntper.Component("cntper6")
	Dim sliderX As ABMSlider=cntperX.Component("sliderper6")
	If SumaSlper=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlper
	End If
End Sub
Sub sliderper7_Changed(value As String)
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim cntperX As ABMContainer=cntper.Component("cntper7")
	Dim sliderX As ABMSlider=cntperX.Component("sliderper7")
	If SumaSlper=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlper
	End If
End Sub
Sub sliderper8_Changed(value As String)
	Dim cntper As ABMContainer=page.Component("cntper")
	Dim cntperX As ABMContainer=cntper.Component("cntper8")
	Dim sliderX As ABMSlider=cntperX.Component("sliderper8")
	If SumaSlper=False Then
		sliderX.SetValue(0)
		sliderX.Refresh
		SumaSlper
	End If
End Sub



Sub creaflotante

	Dim flotante As ABMActionButton
	flotante.Initialize(page, "flotante", "mdi-editor-border-color","", "bigbrown")
	flotante.MainButton.size = ABM.BUTTONSIZE_LARGE
	
	' the sub buttons
	Dim ayuda As ABMButton
	ayuda.InitializeFloating(page, "ayuda", "mdi-action-help", "sub1")
	flotante.AddMenuButton(ayuda)
	
	Dim sms As ABMButton
	sms.InitializeFloating(page, "sms", "mdi-communication-email", "sub2")
	flotante.AddMenuButton(sms)
	
	Dim conf As ABMButton
	conf.InitializeFloating(page, "conf", "mdi-action-settings", "sub3")
	flotante.AddMenuButton(conf)
		
	' add to page
	page.AddActionButton(flotante)

End Sub

Sub flotante_Clicked(Target As String, SubTarget As String)
	If SubTarget="" Then 
		Log("CLICK EN CONF")
		Return
	End If
	
	
	If SubTarget="conf" Then 
		ABMShared.NavigateToPage(ws,ABMPageId,"../configuracion/configuracion.html")
	End If
	
	Dim myTexts, myReturns As List
	myTexts.Initialize
	myReturns.Initialize
	myToastId = myToastId + 1
	page.ShowToast("toast" & myToastId, "toastred", "Clicked on " & SubTarget, 5000)
	Return
End Sub


Sub Page_NavigationbarClicked(Action As String, Value As String)
	page.SaveNavigationBarPosition
	'Abmshared.NavigateToPage(ws, Value)
	
	If Action = "ABMNavigationBar" Then Return
	
	If Action = "salida" Then
		ABMShared.LogOff(page)
		Return
	End If
	
	If Action = "miperfil" Then
		Main.username = ws.Session.GetAttribute2("authName", "")
		Main.subordinado = False
	End If
	
	ABMShared.NavigateToPage(ws,ABMPageId, Value)
	
End Sub

Sub Page_FileUploaded(FileName As String, success As Boolean)	
	
End Sub

Sub Page_ToastClicked(ToastId As String, Action As String)
		
End Sub

Sub Page_ToastDismissed(ToastId As String)	
	
End Sub

Sub Page_Authenticated(Params As Map)
	
End Sub

Sub Page_FirebaseAuthError(extra As String)
	
End Sub

Sub Page_FirebaseAuthStateChanged(IsLoggedIn As Boolean)
	
End Sub

Sub Page_FirebaseStorageError(jobID As String, extra As String)
	
End Sub

Sub Page_FirebaseStorageResult(jobID As String, extra As String)
	
End Sub

Sub Page_ModalSheetDismissed(ModalSheetName As String)
	
End Sub

Sub Page_NextContent(TriggerComponent As String)
	
End Sub

Sub Page_SignedOffSocialNetwork(Network As String, Extra As String)
	
End Sub


Sub Page_Ready()
	Log("Lista alta obj")
	ConnectPage
	page.RestoreNavigationBarPosition
End Sub
